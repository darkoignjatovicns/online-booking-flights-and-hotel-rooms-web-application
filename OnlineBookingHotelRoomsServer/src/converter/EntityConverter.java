package converter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Hotel;
import model.Reservation;
import model.Role;
import model.Room;
import model.RoomStatus;
import model.User;
import service.HotelType;
import service.ReservationType;
import service.RoleType;
import service.RoomStatusType;
import service.RoomType;
import service.UserType;

public class EntityConverter {
	
	@PersistenceContext
	EntityManager em;
	
	
	/* Convert User To UserType */
	public static UserType convertUserToUserType(User user) {
		
		// Covert User to UserType
		UserType userType = new UserType();
		userType.setId(user.getId());
		userType.setFirstName(user.getFirstName());
		userType.setLastName(user.getLastName());
		userType.setEmail(user.getEmail());
		userType.setPassword(user.getPassword());
		userType.setCreatedDate(DateConverter.toXMLGregorianCalendar(user.getCreatedDate()));
		userType.setDeletedDate(DateConverter.toXMLGregorianCalendar(user.getDeletedDate()));
		userType.setLastUpdateDate(DateConverter.toXMLGregorianCalendar(user.getLastUpdateDate()));
		userType.setRole(convertRoleToRoleType(user.getRole()));
		//userType.setImage(user.getImage());
		//userType.setImagePut(user.getImagePut());
	
		return userType;
	}
	
	/* Convert Role to RoleType */
	public static RoleType convertRoleToRoleType(Role role) {
		RoleType roleType = new RoleType();
		roleType.setId(role.getId());
		roleType.setRoleName(role.getRoleName());
		
		return  roleType;
	}
	
	
	/* Convert Reservation To Reservation Type */
	public static ReservationType convertReservationToReservationType(Reservation reservation) {
		
		ReservationType reservationType = new ReservationType();
		
		UserType userType = convertUserToUserType(reservation.getUser());
		RoomType roomType = convertRoomToRoomType(reservation.getRoom());
		
		reservationType.setId(reservation.getId());
		reservationType.setRoom(roomType);
		reservationType.setUser(userType);
		reservationType.setTotalPrice(reservation.getTotalPrice());
		reservationType.setCanceledReservationDate(DateConverter.toXMLGregorianCalendar(reservation.getCanceledReservationDate()));
		reservationType.setCheckInDate(DateConverter.toXMLGregorianCalendar(reservation.getCheckInDate()));
		reservationType.setCheckOutDate(DateConverter.toXMLGregorianCalendar(reservation.getCheckOutDate()));
		reservationType.setReservationDate(DateConverter.toXMLGregorianCalendar(reservation.getReservationDate()));
		return reservationType;
		
	}
	
	/* Convert Room To RoomType */
	public static RoomType convertRoomToRoomType(Room room) {

		RoomType roomType = new RoomType();
		roomType.setAboutRoom(room.getAboutRoom());
		roomType.setAdultsMax(room.getAdultsMax());
		roomType.setChildrenMax(room.getChildrenMax());
		roomType.setCreatedDate(DateConverter.toXMLGregorianCalendar(room.getCreatedDate()));
		roomType.setHotel(convertHotelToHotelType(room.getHotel()));
		roomType.setId(room.getId());
		//roomType.setImage(room.getImage());
		//roomType.setImagePut(room.getImagePut());
		roomType.setRoomTitle(room.getRoomTitle());
		roomType.setPricePerNight(room.getPricePerNight());
		roomType.setRoomStatus(convertRoomStatusToRoomStatusType(room.getRoomStatus()));
		roomType.setLastUpdateDate(DateConverter.toXMLGregorianCalendar(room.getLastUpdateDate()));
		
		return roomType;
		
	}
	
	/* Convert RoomStatus To RoomStatusType */
	public static RoomStatusType convertRoomStatusToRoomStatusType(RoomStatus roomStatus) {
		
		RoomStatusType roomStatusType = new RoomStatusType();
		roomStatusType.setId(roomStatus.getId());
		roomStatusType.setRoomStatus(roomStatus.getRoomStatus());
		
		return roomStatusType;
		
	}
	
	
	/* Convert Hotel to HotelType  */
	public static HotelType convertHotelToHotelType(Hotel hotel) {
		
		HotelType hotelType = new HotelType();
		hotelType.setId(hotel.getId());
		hotelType.setHotelName(hotel.getHotelName());
		hotelType.setCity(hotel.getCity());
		hotelType.setCountry(hotel.getCountry());
		hotelType.setAddress(hotel.getAddress());
		hotelType.setRating((int) hotel.getRating());
		hotelType.setDeletedDate(DateConverter.toXMLGregorianCalendar(hotel.getDeletedDate()));
		hotelType.setLastUpdateDate(DateConverter.toXMLGregorianCalendar(hotel.getDeletedDate()));
		hotelType.setCreatedDate(DateConverter.toXMLGregorianCalendar(hotel.getCreatedDate()));
		//hotelType.setImage(hotel.getImage());
		//hotelType.setImagePut(hotel.getImagePut());
		return hotelType;
		
	}
	
	/* Convert List<Reservation> To List<ReservationType> */
	public static List<ReservationType> convertReservationListToReservationTypeList(List<Reservation> reservations) {
		
		List<ReservationType> reservationTypeList = new ArrayList<>();
		
		for(Reservation reservation : reservations) {
			reservationTypeList.add(convertReservationToReservationType(reservation));
		}
		
		return reservationTypeList;
		
	}
	
	/* Convert List<Room> To List<RoomType> */
	public static List<RoomType> convertRoomListToRoomTypeList(List<Room> rooms) {
		
		List<RoomType> roomTypeList = new ArrayList<>();
		
		for(Room room : rooms) {
			roomTypeList.add(convertRoomToRoomType(room));
		}
		
		return roomTypeList;
		
	}
}
