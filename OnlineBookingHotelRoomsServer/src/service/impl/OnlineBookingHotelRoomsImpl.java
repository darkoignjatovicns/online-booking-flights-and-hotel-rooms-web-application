package service.impl;


import javax.inject.Inject;
import javax.jws.HandlerChain;
import javax.jws.WebService;

import beans.OnlineBookingHotelRoomsBean;
import service.OnlineBookingHotelRooms;

@HandlerChain(file="handler-chain.xml")
@WebService(serviceName = "OnlineBookingHotelRooms", endpointInterface = "service.OnlineBookingHotelRooms", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/")
public class OnlineBookingHotelRoomsImpl implements OnlineBookingHotelRooms {
	
	@Inject
	OnlineBookingHotelRoomsBean onlineBookingHotelRooms;
	
	public service.UserType signUp(java.lang.String firstName, java.lang.String lastName, java.lang.String email,
			java.lang.String username, java.lang.String password) {
		return onlineBookingHotelRooms.signUp(firstName, lastName, email, username, password);
	}

	public java.util.List<service.ReservationType> getAllReservationsByUserEmail(java.lang.String email) {
		return onlineBookingHotelRooms.getAllReservationsByUserEmail(email);
	}

	public service.ReservationType reserveRoom(java.lang.String email, int roomId,
			javax.xml.datatype.XMLGregorianCalendar checkInDate, javax.xml.datatype.XMLGregorianCalendar checkOutDate) {
		return onlineBookingHotelRooms.reserveRoom(email, roomId, checkInDate, checkOutDate);
	}

	public service.UserType signIn(java.lang.String email, java.lang.String password) {
		return onlineBookingHotelRooms.signIn(email, password);
	}

	public java.util.List<service.ReservationType> getAllReservationsByUser(int userId) {
		return onlineBookingHotelRooms.getAllReservationsByUser(userId);
	}

	public service.ReservationType cancelReservation(int reservationId) {
		return onlineBookingHotelRooms.cancelReservation(reservationId);
	}

	public java.util.List<service.RoomType> getFilteredRooms(java.lang.String city, int adults, int children,
			javax.xml.datatype.XMLGregorianCalendar checkIn, javax.xml.datatype.XMLGregorianCalendar checkOut) {
		return onlineBookingHotelRooms.getFilteredRooms(city, adults, children, checkIn, checkOut);
	}
}