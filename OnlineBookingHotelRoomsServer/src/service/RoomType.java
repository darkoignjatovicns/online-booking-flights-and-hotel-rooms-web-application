
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for roomType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="roomType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="hotel" type="{http://www.example.org/OnlineBookingHotelRooms/}hotelType"/&gt;
 *         &lt;element name="roomStatus" type="{http://www.example.org/OnlineBookingHotelRooms/}roomStatusType"/&gt;
 *         &lt;element name="roomTitle" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="adultsMax" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="childrenMax" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="aboutRoom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pricePerNight" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="createdDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="image" type="{http://www.w3.org/2001/XMLSchema}hexBinary"/&gt;
 *         &lt;element name="imagePut" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "roomType", propOrder = {
    "id",
    "hotel",
    "roomStatus",
    "roomTitle",
    "adultsMax",
    "childrenMax",
    "aboutRoom",
    "pricePerNight",
    "lastUpdateDate",
    "createdDate",
    "image",
    "imagePut"
})
public class RoomType {

    protected int id;
    @XmlElement(required = true)
    protected HotelType hotel;
    @XmlElement(required = true)
    protected RoomStatusType roomStatus;
    @XmlElement(required = true)
    protected String roomTitle;
    protected int adultsMax;
    protected int childrenMax;
    @XmlElement(required = true)
    protected String aboutRoom;
    protected int pricePerNight;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastUpdateDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar createdDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(HexBinaryAdapter.class)
    @XmlSchemaType(name = "hexBinary")
    protected byte[] image;
    @XmlElement(required = true)
    protected String imagePut;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the hotel property.
     * 
     * @return
     *     possible object is
     *     {@link HotelType }
     *     
     */
    public HotelType getHotel() {
        return hotel;
    }

    /**
     * Sets the value of the hotel property.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelType }
     *     
     */
    public void setHotel(HotelType value) {
        this.hotel = value;
    }

    /**
     * Gets the value of the roomStatus property.
     * 
     * @return
     *     possible object is
     *     {@link RoomStatusType }
     *     
     */
    public RoomStatusType getRoomStatus() {
        return roomStatus;
    }

    /**
     * Sets the value of the roomStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoomStatusType }
     *     
     */
    public void setRoomStatus(RoomStatusType value) {
        this.roomStatus = value;
    }

    /**
     * Gets the value of the roomTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomTitle() {
        return roomTitle;
    }

    /**
     * Sets the value of the roomTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomTitle(String value) {
        this.roomTitle = value;
    }

    /**
     * Gets the value of the adultsMax property.
     * 
     */
    public int getAdultsMax() {
        return adultsMax;
    }

    /**
     * Sets the value of the adultsMax property.
     * 
     */
    public void setAdultsMax(int value) {
        this.adultsMax = value;
    }

    /**
     * Gets the value of the childrenMax property.
     * 
     */
    public int getChildrenMax() {
        return childrenMax;
    }

    /**
     * Sets the value of the childrenMax property.
     * 
     */
    public void setChildrenMax(int value) {
        this.childrenMax = value;
    }

    /**
     * Gets the value of the aboutRoom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAboutRoom() {
        return aboutRoom;
    }

    /**
     * Sets the value of the aboutRoom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAboutRoom(String value) {
        this.aboutRoom = value;
    }

    /**
     * Gets the value of the pricePerNight property.
     * 
     */
    public int getPricePerNight() {
        return pricePerNight;
    }

    /**
     * Sets the value of the pricePerNight property.
     * 
     */
    public void setPricePerNight(int value) {
        this.pricePerNight = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdateDate(XMLGregorianCalendar value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreatedDate(XMLGregorianCalendar value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImage(byte[] value) {
        this.image = value;
    }

    /**
     * Gets the value of the imagePut property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImagePut() {
        return imagePut;
    }

    /**
     * Sets the value of the imagePut property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImagePut(String value) {
        this.imagePut = value;
    }

}
