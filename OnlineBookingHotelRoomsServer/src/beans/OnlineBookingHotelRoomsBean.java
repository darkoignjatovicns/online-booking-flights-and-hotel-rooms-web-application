package beans;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import converter.EntityConverter;
import model.Reservation;
import model.Role;
import model.Room;
import model.User;
import service.ReservationType;
import service.RoomType;
import service.UserType;

/**
 * Session Bean implementation class OnlineBookingHotelRoomsBean
 */
@Stateless
@LocalBean
public class OnlineBookingHotelRoomsBean implements OnlineBookingHotelRoomsBeanRemote {
	
	@PersistenceContext
	EntityManager em;
	
    /**
     * Default constructor. 
     */
    public OnlineBookingHotelRoomsBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public UserType signUp(String firstName, String lastName, String email, String username, String password) {
		User user = new User();
		user.setCreatedDate(new Date());
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(password);
		user.setRole((Role)em.find(Role.class, 1));
		em.persist(user);
		
		return EntityConverter.convertUserToUserType(user);
	}

	@Override
	public ReservationType reserveRoom(String email, int roomId, XMLGregorianCalendar checkInDate,
			XMLGregorianCalendar checkOutDate) {
		
		Reservation reservation = new Reservation();
		
		//User user = em.find(User.class, userId);
		Query q = em.createQuery("SELECT u FROM User u WHERE u.email = :email");
		q.setParameter("email", email);
		User user = (User) q.getResultList().get(0);
		Room room = em.find(Room.class, roomId);
		
		reservation.setUser(user);
		reservation.setRoom(room);
		reservation.setCheckInDate(DateConverter.toDate(checkInDate));
		reservation.setCheckOutDate(DateConverter.toDate(checkOutDate));
		reservation.setReservationDate(new Date());
		
		em.persist(reservation);
		
		return EntityConverter.convertReservationToReservationType(reservation);
		
	}

	@Override
	public UserType signIn(String email, String password) {
		Query q =  em.createQuery("SELECT u FROM User u WHERE u.email = :email AND u.password = :password");
		q.setParameter("email", email);
		q.setParameter("password", password);
		
		List<User> users =  q.getResultList();
		
		if(users != null) { // Eclipse bug -> There should'n have executed 'if block' if user existed! 
			System.out.println("USERS LIST: " + users);
			User user = users.get(0);
			return EntityConverter.convertUserToUserType(users.get(0));
		}
		
		return null;
		
		
	}

	@Override
	public List<ReservationType> getAllReservationsByUser(int userId) {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE r.user.id = :userId AND r.canceledReservationDate = NULL");
		q.setParameter("userId", userId);
//		/List<Reservation> reservations = q.getResultList();
		return EntityConverter.convertReservationListToReservationTypeList(q.getResultList());
	}

	@Override
	public ReservationType cancelReservation(int reservationId) {
		Reservation reservation = em.find(Reservation.class, reservationId);
		reservation.setCanceledReservationDate(new Date());
		em.merge(reservation);
		return EntityConverter.convertReservationToReservationType(reservation);
	}

	@Override
	public List<RoomType> getFilteredRooms(String city, int adults, int children, XMLGregorianCalendar checkIn,
			XMLGregorianCalendar checkOut) {
			
		Query q = em.createQuery("SELECT r FROM Room r WHERE r.hotel.city LIKE :city AND r.adultsMax >= :adults AND r.childrenMax >= :children"
				+ " AND NOT EXISTS (SELECT res FROM Reservation res WHERE res.room = r"
				+ " AND (:checkIn BETWEEN res.checkInDate AND res.checkOutDate OR :checkOut BETWEEN res.checkInDate AND res.checkOutDate)"
				+ " AND res.canceledReservationDate = NULL)");
		q.setParameter("checkIn", DateConverter.toDate(checkIn));
		q.setParameter("checkOut", DateConverter.toDate(checkOut));
		q.setParameter("city", city);
		q.setParameter("adults", adults);
		q.setParameter("children", children);
		
		return EntityConverter.convertRoomListToRoomTypeList(q.getResultList());
		
	}

	@Override
	public List<ReservationType> getAllReservationsByUserEmail(String email) {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE r.user.email = :email AND r.canceledReservationDate = NULL");
		q.setParameter("email", email);
//		/List<Reservation> reservations = q.getResultList();
		return EntityConverter.convertReservationListToReservationTypeList(q.getResultList());
	}

}
