package beans;

import javax.ejb.Remote;

@Remote
public interface OnlineBookingHotelRoomsBeanRemote {
		
	public service.UserType signUp(java.lang.String firstName, java.lang.String lastName,
			java.lang.String email,
			java.lang.String username, java.lang.String password);

	public service.ReservationType reserveRoom(String email, int roomId,
			javax.xml.datatype.XMLGregorianCalendar checkInDate, 
			javax.xml.datatype.XMLGregorianCalendar checkOutDate);

	public service.UserType signIn(java.lang.String email, java.lang.String password);

	public java.util.List<service.ReservationType> getAllReservationsByUser(int userId);

	public service.ReservationType cancelReservation(int reservationId);

	public java.util.List<service.RoomType> getFilteredRooms(java.lang.String city,
			int adults, int children,
			javax.xml.datatype.XMLGregorianCalendar checkIn,
			javax.xml.datatype.XMLGregorianCalendar checkOut); 
	
	public java.util.List<service.ReservationType> getAllReservationsByUserEmail(java.lang.String email);
	
}
