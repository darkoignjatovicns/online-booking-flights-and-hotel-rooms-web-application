# Online Booking Flight And Hotel Rooms 

Distributed System

### Prerequisites

MySQL Server 5.5.61

PostgreSQL Server 11.3
  
WildFly 14.0.1.Final

Workbench 6.1.3 CE

PgAdmin 4

Eclipse ide Photon June 2018
  
Java jdk 1.8

Node 10.16.0

Npm 6.9.0
 
Angular CLI: 8.0.1

Drivers for database: mysql-connector-java-5.1.40-bin.jar, postgresql-42.2.6.jar
 
### Installing

1. Import sql script in MySQL Workbench and PgAdmin which are in OnlineBookingFlightsAndHotelRoomsDatabase folder

2. Change folder name on server on path \modules\system\layers\base\org\jboss\resteasy\resteasy-json-binding-provider\main\ in mainTMP

3. Install JBoss Tools 4.6.0.Final in Eclipse Marketplace

4. Add WildFly server in Eclipse and press start

5. Add drivers for databases in deployment on WildFly server

6. Add XA datasource for both database

7. Import OnlineBookingFlightsServer, OnlineBookingHotelRoomsServer, OnlineBookingFlightsAndHotelRoomsREST and OnlineBookingFlightsAndHotelRoomsEAR in Eclipse

8. Change JNDI in persistence.xml file in both server projects

9. Deploy EAR file on server

10. Route to OnlineBookingFlightsAndHotelRoomsClient folder and type in terminal

```
npm --install
```

After that 

```
ng serve --open
```

## Deployment

Right click on Server in Eclipse and click "Add and Remove" and add OnlineBookingFlightsAndHotelRoomsEAR then click finish 

## Author

**Darko Ignjatović** 




