import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Reservation } from '../models/Reservation';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }) 
}

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private BASE_URL: string = 'http://localhost:8080/OnlineBookingFlightsAndHotelRoomsREST/rest/onlineBookingHotelRoomsApi';
  private GET_ALL_RESERVATIONS_BY_USER = `${this.BASE_URL}\\getReservationsByUserEmail`;
  private RESERVE_ROOM_URL = `${this.BASE_URL}\\reserveRoom`;
  private CANCEL_RESERVATION_URL = `${this.BASE_URL}\\cancelReservation`;

  constructor(private http: HttpClient) { }

  public getAllReservationsByUser(email: string): Observable<Reservation[]> {

    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');

    let params = new HttpParams();
    params = params.set('email', email);
    return this.http.get<Reservation[]>(this.GET_ALL_RESERVATIONS_BY_USER, { 'headers': headers, 'params': params });
  }

  public reserveRoom(email: string, roomId: string, checkInDate: Date, checkOutDate: Date) : Observable<Reservation> {
 
    return this.http.post<Reservation>(this.RESERVE_ROOM_URL,
          { 'email': email, 'roomId': roomId, 'checkInDate': checkInDate, 'checkOutDate': checkOutDate },
          httpOptions);

  }

  public cancelReservation(reservationId: number) : Observable<Reservation> {
    //return this.http.put<Reservation>(this.CANCEL_RESERVATION_URL,{ 'reservationId': reservationId },httpOptions);
    return this.http.put<Reservation>(this.CANCEL_RESERVATION_URL, reservationId, httpOptions);
  } 

}
