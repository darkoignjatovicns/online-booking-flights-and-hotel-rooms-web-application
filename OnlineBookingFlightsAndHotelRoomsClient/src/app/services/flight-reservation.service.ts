import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { FlightReservation } from '../models/FlightReservation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightReservationService {

  private BASE_ULR:string = "http://localhost:8080/OnlineBookingFlightsAndHotelRoomsREST/rest/onlineBookingFlightsApi"
  private GET_ALL_FLIGHT_RESERVATION_BY_USER_EMAIL = `${this.BASE_ULR}\\getAllFlightReservationByUserEmail`;
  private CANCEL_FLIGHT_RESERVATION_URL = `${this.BASE_ULR}\\cancelFlightReservation`;

  constructor(private http: HttpClient) { }

  public getAllFlightReservationByUserEmail(email: string): Observable<FlightReservation[]> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      params: new HttpParams().set("email",email)
    }

    return this.http.get<FlightReservation[]>(this.GET_ALL_FLIGHT_RESERVATION_BY_USER_EMAIL,httpOptions);

  }

  public cancelFlightReservation(flightReservationId: number) : Observable<FlightReservation> {

    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    }

    return this.http.put<FlightReservation>(this.CANCEL_FLIGHT_RESERVATION_URL,flightReservationId,httpOption);

  }

}
