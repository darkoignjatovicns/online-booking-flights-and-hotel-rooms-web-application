import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Flight } from '../models/Flight';
import { HttpClient, HttpHandler, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  private BASE_URL:string = "http://localhost:8080/OnlineBookingFlightsAndHotelRoomsREST/rest/onlineBookingFlightsApi"
  private GET_ALL_FILTERED_FLIGHTS = `${this.BASE_URL}\\getFilteredFlights`;
  private RESERVE_FLIGHT_URL = `${this.BASE_URL}\\reserveFlight`;


  constructor(private http: HttpClient) { }

  public getFilteredFlights(inputFields):Observable<Flight[]> {

    const httpOption = {
      header: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      params: new HttpParams().set("from", inputFields.from)
                              .set("to", inputFields.to) 
                              .set("departureDate", inputFields.departureDate)
                              .set("adults", inputFields.adults)
                              .set("children", inputFields.children)
    };

    return this.http.get<Flight[]>(this.GET_ALL_FILTERED_FLIGHTS, httpOption);
  }


  public reserveFlight(flightId: number, adults: number, children: number):Observable<Flight> {
    const flightReservation  = {
      flightId: flightId,
      adults: adults,
      children: children,
      email: localStorage.getItem("email")
    }

    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post<Flight>(this.RESERVE_FLIGHT_URL, flightReservation, httpOption);
  }

}
