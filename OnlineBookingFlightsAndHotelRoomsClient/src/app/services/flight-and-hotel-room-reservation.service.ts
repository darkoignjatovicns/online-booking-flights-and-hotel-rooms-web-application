import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FlightReservation } from '../models/FlightReservation';

@Injectable({
  providedIn: 'root'
})
export class FlightAndHotelRoomReservationService {

  private BASE_URL: string = "http://localhost:8080/OnlineBookingFlightsAndHotelRoomsREST/rest/onlineBookingFlightsAndHotelRoomsApi";
  private RESERVE_FLIGHT_AND_ROOM_URL = `${this.BASE_URL}\\reserveFlightAndHotelRoom`;

  constructor(private http: HttpClient) { }

  public reserveFlightAndHotelRoom(flightReservationDetails: any, roomReservationDetails: any) : Observable<any> {
    
    const reservation = {
      'email': localStorage.getItem("email"), 
      'childrenPassengers': flightReservationDetails.children,
      'adultsPassengers': flightReservationDetails.adults,
      'flightId': flightReservationDetails.flightId,

      'checkInDate': roomReservationDetails.checkInDate,
      'checkOutDate': roomReservationDetails.checkOutDate,
      'roomId': roomReservationDetails.roomId,
      'children': roomReservationDetails.children,
      'adults': roomReservationDetails.adults
    }

     const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    return this.http.post<any>(this.RESERVE_FLIGHT_AND_ROOM_URL,reservation,httpOptions);

  }
}
