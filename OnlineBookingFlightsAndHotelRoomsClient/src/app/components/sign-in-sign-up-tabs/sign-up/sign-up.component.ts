import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

   signUpForm: FormGroup;
   submitted: boolean = false;
   emailValid: boolean;
   passwordValid: boolean;
   passwordsMatch: boolean;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private authService: AuthenticationService,
              private router: Router
  ) {
     // Redirect to home if already logged in
     if(this.authService.currentUser()) {
      this.router.navigate(['/']);
     } 
  }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required],
        password2: ['', Validators.required]
    });
    this.emailValid = true;
    this.passwordValid = true;
    this.passwordsMatch = true;
  }

  // convenience getter for easy access to form field
  get f() { return this.signUpForm.controls; }

  onSubmit() {
    this.submitted = true;

    if(!this.validateEmail()) {
      this.emailValid = false;
    } else {
      this.emailValid = true;
    }

    if(!this.validatePassword()) {
      this.passwordValid = false;
    } else {
      this.passwordValid = true;
    }

    // stop here if form is invalid
    if(this.signUpForm.invalid || (!this.emailValid || !this.passwordValid || !this.passwordsMatch)) {
      return;
    }

    let user: User = new User();
    user.firstName = this.f.firstName.value;
    user.lastName = this.f.lastName.value;
    user.email = this.f.email.value;
    user.username = this.f.username.value;
    user.password = this.f.password.value;

    this.userService.signUp(user).subscribe
      (
        (res) => {
          //this.alertService("You have successfully registred");
          console.log(res);
          this.router.navigate(['/signIn']);
        }, 
        error => {
          //this.alertService.error(error.error);
          alert("Error has occured while singingUp");
        } 
      )

  }

  checkPasswords(): void {
    if(this.signUpForm.controls.password.value != this.signUpForm.controls.password2.value && this.signUpForm.controls.password2.value !== "") {
      this.passwordsMatch = false;
    } else {
      this.passwordsMatch = true;
    }
  }

  validateEmail(): boolean {
    let regExp: RegExp = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))){2,6}$/i;
    return regExp.test(this.signUpForm.controls.email.value);
  }

  validatePassword() :boolean {
    let regExp: RegExp = /^(?=.*[!"#$%&'()*+,-./:;<=>?@\[\]\^_`{|}~])(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$/;
    return regExp.test(this.signUpForm.controls.password.value);  
  }

  clearValid() {
    this.passwordValid = true;
  }

  clearEmailValid() {
    this.emailValid = true;
  }

}
