import { Component, OnInit } from '@angular/core';
import { FlightReservation } from 'src/app/models/FlightReservation';
import { FlightReservationService } from 'src/app/services/flight-reservation.service';

@Component({
  selector: 'app-flight-reservations',
  templateUrl: './flight-reservations.component.html',
  styleUrls: ['./flight-reservations.component.css']
})
export class FlightReservationsComponent implements OnInit {

  flightReservations: Array<FlightReservation> = [];

  constructor(private flightReservationService: FlightReservationService) { }

  ngOnInit() {
    this.loadFlightReservations();
  }


  private loadFlightReservations() {
    this.flightReservationService.getAllFlightReservationByUserEmail(localStorage.getItem("email"))
    .subscribe(
      (res: FlightReservation[]) => {
        this.flightReservations = res;
        
      },
      error => {
        alert("Error has occured while getting flight reservations");
        
      }
    )
  }

  cancelFlightReservation(flightReservationId: number) {

    // Delete from UI
    this.flightReservations = this.flightReservations.filter( r => r.id != flightReservationId);
    // Delete from server
    this.flightReservationService.cancelFlightReservation(flightReservationId).subscribe
    (
      (res: FlightReservation) => {

      },
      error => {
        alert("Error has occured while canceling flight reservation");
      }
    )
  }

}
