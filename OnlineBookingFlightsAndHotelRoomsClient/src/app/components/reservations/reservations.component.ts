import { Component, OnInit } from '@angular/core';
import { Reservation } from 'src/app/models/Reservation';
import { ReservationService } from 'src/app/services/reservation.service';
import { User } from 'src/app/models/User';
import { FlightReservation } from 'src/app/models/FlightReservation';
import { FlightReservationService } from 'src/app/services/flight-reservation.service';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservations: Array<Reservation> = [];
  //flightReservations: Array<FlightReservation> = [];

  constructor(private reservationService: ReservationService,
              private flightReservationsService: FlightReservationService
  ) { }

  ngOnInit() {
    this.loadReservation();
    //this.loadFlightReservations();
  }

  private loadReservation() {
    this.reservationService.getAllReservationsByUser(localStorage.getItem('email')).subscribe
    (
      (res: Reservation[]) => {
        this.reservations = res;
  
      },
      error => {
        alert("Error has occured while getting reservations");
      }
    )
  }

 /* private loadFlightReservations() {
    this.flightReservationsService.getAllFlightReservationByUserEmail(localStorage.getItem("email"))
    .subscribe(
      (res: FlightReservation[]) => {
        this.flightReservations = res;
      },
      error => {
        alert("Error has occured while getting flight reservations");
      }
    )
  }*/


  cancelReservation(reservationId: number) {

    // Delete from UI
    this.reservations = this.reservations.filter(reservation => reservation.id != reservationId);
    //Delete from server
    this.reservationService.cancelReservation(reservationId).subscribe
    (
      (res: Reservation) => {
        //alert('Reservation have successfully canceled');
      },
      error => {
        alert("Error has occured while canceling reservation");
      }
    )
  }

}
