import { Component, OnInit } from '@angular/core';
import { FlightAndHotelRoomReservationService } from 'src/app/services/flight-and-hotel-room-reservation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {

  flightResDetails: any = null;
  roomResDetails: any = null;
  btnVisible: boolean = false;

  constructor(private flightAndHotelRoomReservationService: FlightAndHotelRoomReservationService,
              private router: Router
  ) { }

  ngOnInit() {
  }

  flightReservationDetails(flightReservation: any) {
    this.flightResDetails = flightReservation;
    this.checkIfCheckedFlightAndHotel();
  }

  hotelRoomReservationDetails(hotelRoomReservation: any) {
    this.roomResDetails = hotelRoomReservation;
    this.checkIfCheckedFlightAndHotel();
  }

  private checkIfCheckedFlightAndHotel() {
    if(this.flightResDetails != null && this.hotelRoomReservationDetails != null) {
      this.btnVisible = true;
    }
  }

  reserveFlightAndHotel() {
    if(this.flightResDetails != null && this.hotelRoomReservationDetails != null) {
      
        this.flightAndHotelRoomReservationService.reserveFlightAndHotelRoom(this.flightResDetails,this.roomResDetails)
        .subscribe(
          (res: any) => {
            this.router.navigate(['/reservations']);
          },
          error => {
            //alert("Error has occured while making flight and hotel room reservation");
            this.router.navigate(['/reservations']); // Bug
          }
        )
    }
  }

}
