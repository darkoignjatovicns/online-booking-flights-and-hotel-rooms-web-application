import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Room } from 'src/app/models/Room';

@Component({
  selector: 'app-room-item',
  templateUrl: './room-item.component.html',
  styleUrls: ['./room-item.component.css']
})
export class RoomItemComponent implements OnInit {

  @Input() room: Room;
  @Output() checkRoom :EventEmitter<any> = new EventEmitter();
  @Output() reserveRoom :EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }


  // Set Dynamic Classes
  setClasses() {
    let classes = {
      room: true,
      'is-checked': this.room.checked
    }

    return classes;
  }

  onToggleHotelRoom() {
    this.room.checked = !this.room.checked;
  }

  onCheckRoom(room: Room) {
    this.checkRoom.emit(room);
  }

  onReserveRoom(roomId: number) {
    this.reserveRoom.emit(roomId);
  }

}
