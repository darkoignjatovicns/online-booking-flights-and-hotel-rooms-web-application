import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Room } from 'src/app/models/Room';
import { RoomService } from 'src/app/services/room.service';
import { ReservationService } from 'src/app/services/reservation.service';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/models/Reservation';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css'],
  animations: [
    // Definitions of animations triggers

    trigger('showFilterHotelRooms', [
        state('visible', style({  visibility: 'visible' })),
        state('nonvisible', style ({  visibility: 'hidden' })),
        transition('visible <=> nonvisible', animate('1ms'))
    ]),
  ]
})
export class RoomsComponent implements OnInit {

  @Output() hotelRoomReservationDetails: EventEmitter<any> = new EventEmitter();

  filteredRooms: Room[] = [];
  checkInDate: Date = null;
  checkOutDate: Date = null;
  searchTerm: string;
  adults: number;
  children: number;
  checkedRooms: Array<Room> = new Array();

  
  changeCheckedStatus(room: Room) {
    // If current checked rooms is empty, change room checked status and push in array
    if(this.checkedRooms.length == 0) {
      room.checked = !room.checked;
      this.checkedRooms.push(room);
    } else {
      // If room is equal room which is in array then can change room checked status
      if(room.id === this.checkedRooms[0].id) {
        // Change room checked status
        room.checked = !room.checked;
        this.checkedRooms.pop();   
      } else {
        alert("You're allowed to choose only one room and flight");
      } 
    } 
  }

  // Show/Hide -  Filter form for rooms
  filterHotelRoomSstate: string = 'nonvisible';
  btnText: string = 'Booking Hotel Rooms';

  onToggle() {
    this.filterHotelRoomSstate = (this.filterHotelRoomSstate === 'nonvisible' ? 'visible' : 'nonvisible');
    console.log(this.filterHotelRoomSstate);
    this.btnText = (this.filterHotelRoomSstate === 'nonvisible' ? 'Book hotel rooms' : 'Hide');
  }
  
  constructor(private roomService: RoomService,
              private reservationService: ReservationService,
              private router: Router 
  ) { }

  ngOnInit() {

  }

 

  filterRooms(inputFields: any) {

    this.roomService.getFilteredRooms(inputFields).subscribe
    (
      (res: Room[]) => {
        this.filteredRooms = res;
        this.checkInDate = inputFields.checkIn;
        this.checkOutDate = inputFields.checkOut;
        this.adults = inputFields.adults;
        this.children = inputFields.children;
      },
      error => {
        alert("Error has occured while getting rooms");
      }
    )
  }

  reserveRoom(roomId: number,checkInDate: Date, checkOutDate: Date) {

    this.reservationService.reserveRoom(localStorage.getItem('email'), String(roomId), this.checkInDate, this.checkOutDate).subscribe
    (
      (res: Reservation) => {
        this.router.navigate(['/reservations']);
      },
      error => {
        alert("Error has occured while making reservation");
      }
    )
  }

  checkRoom(room: Room) {

    if(!room.checked) {
      return;
    }
  
    console.log("Here");
   const roomReservation = {
      'roomId': room.id,
      'adults': this.adults,
      'children': this.children,
      'email': localStorage.getItem("email"), 
      'checkInDate': this.checkInDate,
      'checkOutDate': this.checkOutDate,
    }

    this.hotelRoomReservationDetails.emit(roomReservation);
    

  }

}
