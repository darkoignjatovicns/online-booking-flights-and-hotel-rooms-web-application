import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-flight-filter',
  templateUrl: './flight-filter.component.html',
  styleUrls: ['./flight-filter.component.css']
})
export class FlightFilterComponent implements OnInit {

  @Output() filterFlights: EventEmitter<any> = new EventEmitter();

  flightFilterForm: FormGroup;
  submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.flightFilterForm = this.formBuilder.group({
      from: ['', Validators.required],
      to: ['', Validators.required],
      departureDate: ['', Validators.required],
      adults: [2, Validators.required],
      children: [0, Validators.required]
    })
  }

  get f() { return this.flightFilterForm.controls; }

  onSubmit() {

    this.submitted = true;

    if(this.flightFilterForm.invalid) {
      return;
    }


    const filterFields = {
      from: this.f.from.value,
      to: this.f.to.value,
      departureDate: this.f.departureDate.value,
      adults: this.f.adults.value,
      children: this.f.children.value
    }

    console.log(filterFields.from);
    this.filterFlights.emit(filterFields);


  }

}
