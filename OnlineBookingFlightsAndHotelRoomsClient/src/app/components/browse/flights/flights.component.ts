import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Flight } from 'src/app/models/Flight';
import { FlightService } from 'src/app/services/flight.service';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { getCurrencySymbol } from '@angular/common';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css'],
  animations: [
    // Definitions of animations triggers

    trigger('checkFlightBtn', [
      state('unchecked', style({ background: 'none'})),
      state('checked', style({ background: 'green'})),
      transition('checked <=> unchecked', animate('1ms'))
    ])

  ]
})
export class FlightsComponent implements OnInit {

  @Output() flightReservationDetails: EventEmitter<any> = new EventEmitter();

  filteredFlights: Array<Flight> = [];
  adults: number;
  children: number;
 // flightId: number;

  checkedText: string = 'Check flight';
  uncheckText: string = 'Uncheck flight';
  checkFlightBtnState: string = 'unchecked';
  checkFlightBtnText: string = this.checkedText;

  onToggleFlight() {
    this.checkFlightBtnState = (this.checkFlightBtnState === 'unchecked' ? 'checked' : 'unchecked');
    console.log(this.checkFlightBtnState);
    this.checkFlightBtnText = (this.checkFlightBtnState === 'unchecked' ? this.checkedText : this.uncheckText);
  }
  

  constructor(private flightService: FlightService,
              private router: Router
  ) { }

  ngOnInit() {
  }

  filterFlights(inputFileds: any) {
    this.flightService.getFilteredFlights(inputFileds).subscribe(
      (res: Flight[]) => {
        this.filteredFlights = res;
        this.adults = inputFileds.adults;
        this.children = inputFileds.children;
      }, 
      error => {
        alert("Error has occured while getting flights");
      }
    )
      
  }   


  reserveFlight(flightId: number) {
    this.flightService.reserveFlight(flightId, this.adults, this.children).subscribe
    (
      (res: Flight) => {
        this.router.navigate(['/reservations']);
      }, 
      error => {
        alert("Error has occured while reserving flight");
      }

    )
  }

  checkFlight(flightId: number) {

    if(this.checkFlightBtnState === 'unchecked') {
      return;
    } 

   const flightReservation = {
      'flightId': flightId,
      'adults': this.adults,
      'children': this.children,
      'email': localStorage.getItem("email")
    }

    this.flightReservationDetails.emit(flightReservation);
    

  }

}
