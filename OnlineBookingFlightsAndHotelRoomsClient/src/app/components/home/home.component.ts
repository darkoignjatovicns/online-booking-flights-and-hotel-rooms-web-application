import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title: string = "Welcome to online booking hotel rooms";
  userInfo: string = "";


  constructor(private authService: AuthenticationService,
              private router: Router
  ) {
      /*if(!this.authService.currentUser()) {
      this.router.navigate(['/signIn']);
      return;
    }  */
  }

  ngOnInit() {
    let firstName: string = localStorage.getItem("firstName");
    let lastName: string = localStorage.getItem("lastName");
    let roleName: string = localStorage.getItem("role");
    if(firstName != null) {
      this.userInfo = firstName + " " + lastName + "(" + roleName + ")";
    }
  }

}
