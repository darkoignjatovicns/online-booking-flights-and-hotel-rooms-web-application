import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavigationComponent } from './components/layout/navigation/navigation.component';
import { SignUpComponent } from './components/sign-in-sign-up-tabs/sign-up/sign-up.component';
import { SignInSignUpTabsComponent } from './components/sign-in-sign-up-tabs/sign-in-sign-up-tabs.component';
import { SignInComponent } from './components/sign-in-sign-up-tabs/sign-in/sign-in.component';
import { LogoutComponent } from './components/logout/logout.component';

import { ReservationsComponent } from './components/reservations/reservations.component';
import { RoomSearchTermFilterPipe } from './shared/room-search-term-filter.pipe';
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { FlightReservationsComponent } from './components/reservations/flight-reservations/flight-reservations.component';
import { BrowseComponent } from './components/browse/browse.component';
import { FlightsComponent } from './components/browse/flights/flights.component';
import { FlightFilterComponent } from './components/browse/flights/flight-filter/flight-filter.component';
import { RoomsComponent } from './components/browse/rooms/rooms.component';
import { RoomFilterComponent } from './components/browse/rooms/room-filter/room-filter.component';
import { RoomItemComponent } from './components/browse/rooms/room-item/room-item.component';


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomeComponent,
    NavigationComponent,
    SignUpComponent,
    SignInSignUpTabsComponent,
    RoomsComponent,
    LogoutComponent,
    RoomFilterComponent,
    RoomItemComponent,
    ReservationsComponent,
    RoomSearchTermFilterPipe,
    FlightReservationsComponent,
    BrowseComponent,
    FlightsComponent,
    FlightFilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
