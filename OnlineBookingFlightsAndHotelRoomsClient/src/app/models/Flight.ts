export class Flight {

    id:number;
    departureDate:Date;
    departureTime:Date;
    expectedTravelTime:Date;
    from:string;
    to:string;

    constructor(){}

}