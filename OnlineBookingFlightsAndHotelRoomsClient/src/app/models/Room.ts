import { Hotel } from './Hotel';
import { RoomStatus } from './RoomStatus';
import { ButtonState } from './ButtonState';

export class Room extends ButtonState{
    id: number;
    roomTitle: string;
    adultsMax: number;
    childrenMax: number;
    aboutRoom: string;
    pricePerNight: number;
    roomStatus: RoomStatus;
    hotel: Hotel;

    constructor() {
        super();
    }

}