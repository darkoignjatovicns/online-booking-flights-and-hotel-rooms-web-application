import { Room } from './Room';
import { User } from './User';

export class Reservation {
    id: number;
    checkInDate: Date;
    checkOutDate: Date;
    room: Room;
    user: User;
    totalPrice: number;

    userId: number;
    roomId: number;

    constructor(){}

    public setUserId(userId: number) {
        this.userId = userId;
    }


    public setRoomId(roomId: number) {
        this.roomId = roomId;
    }

    public setCheckInDate(checkInDate: Date) {
        this.checkInDate = checkInDate;
    }

    public setCheckOutDate(checkOutDate: Date) {
        this.checkOutDate = checkOutDate;
    }

}

