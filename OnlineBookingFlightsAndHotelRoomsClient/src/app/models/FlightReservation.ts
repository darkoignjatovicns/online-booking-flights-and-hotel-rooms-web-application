import { Flight } from './Flight';

export class FlightReservation {
    id:number;
    adults:number;
    children:number;
    reservationDate:Date;
    totalPrice:number;
    flight:Flight;
}