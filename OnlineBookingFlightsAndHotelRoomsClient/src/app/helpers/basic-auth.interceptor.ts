import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const token = localStorage.getItem("token");

        if(token) {
            const cloned = req.clone({
                headers: req.headers.set(
                    "Authorization", "Basic " + token
                )
            });
            
            return next.handle(cloned);
        } else {
            return next.handle(req);
        }

    }




}