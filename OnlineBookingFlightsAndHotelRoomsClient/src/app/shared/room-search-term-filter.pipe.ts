import { Pipe, PipeTransform } from '@angular/core';
import { Room } from '../models/Room';

@Pipe({
  name: 'roomSearchTermFilter'
})
export class RoomSearchTermFilterPipe implements PipeTransform {

  transform(rooms: Room[], searchTerm: string): Room[] {

    if(searchTerm == null || searchTerm === "") {
      return rooms;
    }
    return rooms.filter(r => r.hotel.hotelName.toLowerCase().includes(searchTerm.toLowerCase()));
    
  }

}
