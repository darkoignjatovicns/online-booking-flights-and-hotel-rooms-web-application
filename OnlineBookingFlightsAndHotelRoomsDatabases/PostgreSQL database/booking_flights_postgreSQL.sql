--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-06-25 13:52:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16428)
-- Name: flight; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flight (
    plane_id integer,
    "from" character(255),
    "to" character(255),
    departure_date date,
    departure_time time without time zone,
    expected_travel_time time without time zone,
    return_date date,
    return_time time without time zone,
    price integer,
    id integer NOT NULL
);


ALTER TABLE public.flight OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16531)
-- Name: flight_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.flight_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flight_id_seq OWNER TO postgres;

--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 205
-- Name: flight_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.flight_id_seq OWNED BY public.flight.id;


--
-- TOC entry 198 (class 1259 OID 16420)
-- Name: plane; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plane (
    name character(255),
    type character(255),
    company character(255),
    "numberOfSeats" integer,
    id integer NOT NULL
);


ALTER TABLE public.plane OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16519)
-- Name: plane_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.plane_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plane_id_seq OWNER TO postgres;

--
-- TOC entry 2872 (class 0 OID 0)
-- Dependencies: 204
-- Name: plane_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.plane_id_seq OWNED BY public.plane.id;


--
-- TOC entry 200 (class 1259 OID 16442)
-- Name: reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservation (
    user_id integer,
    flight_id integer,
    reservation_date date,
    check_in_date date,
    canceled_reservation date,
    adults integer,
    total_price integer,
    children integer,
    id integer NOT NULL
);


ALTER TABLE public.reservation OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16508)
-- Name: reservation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_id_seq OWNER TO postgres;

--
-- TOC entry 2873 (class 0 OID 0)
-- Dependencies: 203
-- Name: reservation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservation_id_seq OWNED BY public.reservation.id;


--
-- TOC entry 197 (class 1259 OID 16403)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    role_name character(20),
    id integer NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16499)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 2874 (class 0 OID 0)
-- Dependencies: 202
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 196 (class 1259 OID 16395)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    first_name character(255),
    last_name character(255),
    email character(255),
    username character(255),
    password character(255),
    created_date date,
    image bytea,
    "imagePut" character(1024),
    last_update_date date,
    deleted_date date,
    role_id integer,
    id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16486)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 2875 (class 0 OID 0)
-- Dependencies: 201
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 2715 (class 2604 OID 16533)
-- Name: flight id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight ALTER COLUMN id SET DEFAULT nextval('public.flight_id_seq'::regclass);


--
-- TOC entry 2714 (class 2604 OID 16521)
-- Name: plane id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plane ALTER COLUMN id SET DEFAULT nextval('public.plane_id_seq'::regclass);


--
-- TOC entry 2716 (class 2604 OID 16510)
-- Name: reservation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation ALTER COLUMN id SET DEFAULT nextval('public.reservation_id_seq'::regclass);


--
-- TOC entry 2713 (class 2604 OID 16501)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 2712 (class 2604 OID 16488)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 2859 (class 0 OID 16428)
-- Dependencies: 199
-- Data for Name: flight; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.flight (plane_id, "from", "to", departure_date, departure_time, expected_travel_time, return_date, return_time, price, id) FROM stdin;
\.


--
-- TOC entry 2858 (class 0 OID 16420)
-- Dependencies: 198
-- Data for Name: plane; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.plane (name, type, company, "numberOfSeats", id) FROM stdin;
\.


--
-- TOC entry 2860 (class 0 OID 16442)
-- Dependencies: 200
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservation (user_id, flight_id, reservation_date, check_in_date, canceled_reservation, adults, total_price, children, id) FROM stdin;
\.


--
-- TOC entry 2857 (class 0 OID 16403)
-- Dependencies: 197
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (role_name, id) FROM stdin;
User                	1
Manager             	2
\.


--
-- TOC entry 2856 (class 0 OID 16395)
-- Dependencies: 196
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (first_name, last_name, email, username, password, created_date, image, "imagePut", last_update_date, deleted_date, role_id, id) FROM stdin;
John                                                                                                                                                                                                                                                           	Doe                                                                                                                                                                                                                                                            	john@gmail.com                                                                                                                                                                                                                                                 	john                                                                                                                                                                                                                                                           	John021.                                                                                                                                                                                                                                                       	\N	\N	\N	\N	\N	1	1
Ashley                                                                                                                                                                                                                                                         	Nicolette                                                                                                                                                                                                                                                      	ashley@gmail.com                                                                                                                                                                                                                                               	ashley                                                                                                                                                                                                                                                         	Ashley021.                                                                                                                                                                                                                                                     	\N	\N	\N	\N	\N	\N	2
\.


--
-- TOC entry 2876 (class 0 OID 0)
-- Dependencies: 205
-- Name: flight_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.flight_id_seq', 1, false);


--
-- TOC entry 2877 (class 0 OID 0)
-- Dependencies: 204
-- Name: plane_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.plane_id_seq', 1, false);


--
-- TOC entry 2878 (class 0 OID 0)
-- Dependencies: 203
-- Name: reservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservation_id_seq', 1, false);


--
-- TOC entry 2879 (class 0 OID 0)
-- Dependencies: 202
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 2, true);


--
-- TOC entry 2880 (class 0 OID 0)
-- Dependencies: 201
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 2, true);


--
-- TOC entry 2725 (class 2606 OID 16535)
-- Name: flight flight_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight
    ADD CONSTRAINT flight_pkey PRIMARY KEY (id);


--
-- TOC entry 2723 (class 2606 OID 16523)
-- Name: plane plane_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plane
    ADD CONSTRAINT plane_pkey PRIMARY KEY (id);


--
-- TOC entry 2729 (class 2606 OID 16512)
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- TOC entry 2721 (class 2606 OID 16503)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2719 (class 2606 OID 16490)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2727 (class 1259 OID 16458)
-- Name: flight_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX flight_id_fkey ON public.reservation USING btree (flight_id);


--
-- TOC entry 2726 (class 1259 OID 16441)
-- Name: plane_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX plane_id_fkey ON public.flight USING btree (plane_id);


--
-- TOC entry 2717 (class 1259 OID 16419)
-- Name: role_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX role_id_fkey ON public."user" USING btree (role_id);


--
-- TOC entry 2730 (class 1259 OID 16452)
-- Name: user_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_id_fkey ON public.reservation USING btree (user_id);


--
-- TOC entry 2732 (class 2606 OID 16560)
-- Name: flight flight_plane_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight
    ADD CONSTRAINT flight_plane_id_fkey FOREIGN KEY (plane_id) REFERENCES public.plane(id);


--
-- TOC entry 2734 (class 2606 OID 16555)
-- Name: reservation reservation_flight_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES public.flight(id);


--
-- TOC entry 2733 (class 2606 OID 16550)
-- Name: reservation reservation_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 2731 (class 2606 OID 16545)
-- Name: user user_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.role(id);


-- Completed on 2019-06-25 13:52:23

--
-- PostgreSQL database dump complete
--

