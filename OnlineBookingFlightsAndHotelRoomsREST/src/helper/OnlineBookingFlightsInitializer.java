package helper;


import onlineBookingFlightsService.OnlineBookingFlights;
import onlineBookingFlightsService.OnlineBookingFlights_Service;


public class OnlineBookingFlightsInitializer {
	
	private static OnlineBookingFlights port1;
	
	static {
		OnlineBookingFlights_Service service1 = new OnlineBookingFlights_Service();
		//service1.setHandlerResolver(new CustomBookingFlightsHandlerResolver());		
		port1 = service1.getOnlineBookingFlightsImplPort(); 
	}
	
	public static OnlineBookingFlights getOnlineBookingFlightsPort() {
		return port1;
	}
	
	
	
}
