package helper;

import handler.CustomHandlerResolver;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms_Service;


public class Initializer {
	
	
	
	public static OnlineBookingHotelRooms getOnlineBookingHotelRoomsPort() {
		OnlineBookingHotelRooms_Service service1 = new OnlineBookingHotelRooms_Service();
		service1.setHandlerResolver(new CustomHandlerResolver());		
		OnlineBookingHotelRooms port1 = service1.getOnlineBookingHotelRoomsImplPort();
		
		/*Client client = ClientProxy.getClient(port1);
		Endpoint cxfEndpoint = client.getEndpoint();

		Map<String, Object> outProps = new HashMap<String, Object>();
		outProps.put("action", "Encrypt Signature Timestamp");
		outProps.put("user", "client");
		outProps.put("passwordCallbackClass", "handlers.ClientKeystorePasswordCallback");
		outProps.put("encryptionPropFile", "resources/clientKeystore.properties");
		outProps.put("encryptionUser", "server");
		outProps.put("encryptionKeyIdentifier", "DirectReference");
		outProps.put("signatureKeyIdentifier", "DirectReference");
		outProps.put("signaturePropFile", "resources/clientKeystore.properties");
		outProps.put("signatureParts",
				"{}{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Timestamp;{}{http://schemas.xmlsoap.org/soap/envelope/}Body;");
		

		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps); // request
		cxfEndpoint.getOutInterceptors().add(wssOut);

		
		
		Map<String, Object> inProps = new HashMap<String, Object>();
		inProps.put("action", "Encrypt");
		inProps.put("passwordCallbackClass", "handlers.ClientKeystorePasswordCallback");
		inProps.put("decryptionPropFile", "resources/clientKeystore.properties");
		WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps); // response
		 cxfEndpoint.getInInterceptors().add(wssIn);*/
		 
		return port1;
	}
	
	
}
