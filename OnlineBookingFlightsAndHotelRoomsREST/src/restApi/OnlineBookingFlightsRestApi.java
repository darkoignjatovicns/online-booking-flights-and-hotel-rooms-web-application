package restApi;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import dto.FlightReservationDTO;
import dto.UserDTO;
import helper.OnlineBookingFlightsInitializer;
import onlineBookingFlightsService.FlightReservationType;
import onlineBookingFlightsService.FlightType;
import onlineBookingFlightsService.OnlineBookingFlights;
import onlineBookingFlightsService.UserType;

@Path("/onlineBookingFlightsApi")
public class OnlineBookingFlightsRestApi {

	
	OnlineBookingFlights port1 = OnlineBookingFlightsInitializer.getOnlineBookingFlightsPort();
	
	
	@GET
	@Path("/signIn")
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signIn(@Context HttpServletRequest request) {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		return port1.signIn(email, password);
	}
	
	@POST
	@Path("/signUp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signUp(UserDTO userDto) {
		String firstName = userDto.getFirstName();
		String lastName = userDto.getLastName();
		String username = userDto.getUsername();
		String password = userDto.getPassword();
		String email = userDto.getEmail();
		return port1.signUp(firstName, lastName, email, username, password);
	}
	
	@GET
	@Path("/getFilteredFlights")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<FlightType> getFilteredFlights(@Context HttpServletRequest request) {
		
		int children = Integer.parseInt(request.getParameter("children"));
		int adults = Integer.parseInt(request.getParameter("adults"));
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		XMLGregorianCalendar departureDate = null;
		//XMLGregorianCalendar returnDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			departureDate = DateConverter.toXMLGregorianCalendar(sdf.parse(request.getParameter("departureDate")));
//			if(!request.getParameter("returnDate").equals("null")) {
//				returnDate = DateConverter.toXMLGregorianCalendar(sdf.parse(request.getParameter("returnDate")));
//			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return port1.getFilteredFlights(from, to, departureDate, null);
		
	}
	
	
	@POST
	@Path("/reserveFlight")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public FlightReservationType reserveFlight(FlightReservationDTO flightReservationDto) {
		int adults = flightReservationDto.getAdults();
		int children = flightReservationDto.getChildren();
		String email = flightReservationDto.getEmail();
		int flightId = flightReservationDto.getFlightId();
		return port1.reserveFlight(email, flightId, adults, children);
	}
	
	@GET
	@Path("/getAllFlightReservationByUser")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<FlightReservationType> getAllFlightReservationByUser(@Context HttpServletRequest request) {
		int userId = Integer.parseInt(request.getParameter("userId"));
		return port1.getAllReservedFlightsByUser(userId);
	}
	
	@GET
	@Path("/getAllFlightReservationByUserEmail")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<FlightReservationType> getAllFlightReservationByUserEmail(@Context HttpServletRequest request) {
		String email = request.getParameter("email");
		return port1.getAllFlightReservationByUserEmail(email);
	}
	
	@PUT
	@Path("/cancelFlightReservation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public FlightReservationType cancelFlightReservation(int flightReservationId) {
		return port1.cancelFlightReservation(flightReservationId);
	}
}
