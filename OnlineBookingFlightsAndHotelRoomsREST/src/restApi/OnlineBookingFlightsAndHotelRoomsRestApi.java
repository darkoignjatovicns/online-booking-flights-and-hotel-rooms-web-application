package restApi;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import dto.FlightAndHotelRoomReservationDTO;
import dto.UserDTO;
import helper.Initializer;
import helper.OnlineBookingFlightsInitializer;
import onlineBookingFlightsService.OnlineBookingFlights;
import onlineBookingFlightsService.UserType;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms;



@Path("/onlineBookingFlightsAndHotelRoomsApi")
public class OnlineBookingFlightsAndHotelRoomsRestApi {
	
	
	OnlineBookingFlights port1 = OnlineBookingFlightsInitializer.getOnlineBookingFlightsPort();
	private final OnlineBookingHotelRooms port2= Initializer.getOnlineBookingHotelRoomsPort();
	
	
	@GET
	@Path("/signIn")
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signIn(@Context HttpServletRequest request) {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		//port2.signIn(email, password);
		return port1.signIn(email, password);
	}
	
	@POST
	@Path("/signUp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signUp(UserDTO userDto) {
		String firstName = userDto.getFirstName();
		String lastName = userDto.getLastName();
		String username = userDto.getUsername();
		String password = userDto.getPassword();
		String email = userDto.getEmail();
		port2.signUp(firstName, lastName, email, username, password);
		return port1.signUp(firstName, lastName, email, username, password);
	}
	

	
	@POST
	@Path("/reserveFlightAndHotelRoom")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public Response reserveFlightAndHotelRoom(FlightAndHotelRoomReservationDTO reservationDto) {
		
		String email = reservationDto.getEmail();
		
		// Flight
		int adultsPassengers = reservationDto.getAdultsPassengers();
		int childrenPassengers = reservationDto.getChildrenPassengers();
		int flightId = reservationDto.getFlightId();
		port1.reserveFlight(email, flightId, adultsPassengers, childrenPassengers);
		
		// Hotel room
		int roomId = reservationDto.getRoomId();
		XMLGregorianCalendar checkInDate = null, checkOutDate = null;
		try {
			checkInDate = DateConverter.toXMLGregorianCalendar(reservationDto.getCheckInDate());
			checkOutDate = DateConverter.toXMLGregorianCalendar(reservationDto.getCheckOutDate());
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		port2.reserveRoom(email, roomId, checkInDate, checkOutDate);
		
		return Response.ok("Successfuly").build();
	}
	
}
