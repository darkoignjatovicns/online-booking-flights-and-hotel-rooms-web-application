package restApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import dto.ReservationDTO;
import dto.UserDTO;
import helper.Initializer;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms;
import onlineBookingHotelRoomsService.ReservationType;
import onlineBookingHotelRoomsService.RoomType;
import onlineBookingHotelRoomsService.UserType;


@Path("/onlineBookingHotelRoomsApi")
public class OnlineBookingHotelRoomsRestApi {
	
	private final OnlineBookingHotelRooms port1 = Initializer.getOnlineBookingHotelRoomsPort();
	
	
	@GET
	@Path("/signIn")
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signIn(@Context HttpServletRequest request) {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		return port1.signIn(email, password);
	}
	
	@POST
	@Path("/signUp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll
	public UserType signUp(UserDTO userDto) {
		String firstName = userDto.getFirstName();
		String lastName = userDto.getLastName();
		String username = userDto.getUsername();
		String password = userDto.getPassword();
		String email = userDto.getEmail();
		return port1.signUp(firstName, lastName, email, username, password);
	}
	
	
	@GET
	@Path("/getFilteredRooms")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<RoomType> getFilteredRooms(@Context HttpServletRequest request) {
		
		String city = request.getParameter("city");
		int adults = Integer.parseInt(request.getParameter("adults"));
		int children = Integer.parseInt(request.getParameter("children"));
		DateFormat format = new SimpleDateFormat("yyyyy-MM-dd");
		XMLGregorianCalendar checkIn = null, checkOut = null;
		try {
			checkIn = DateConverter.toXMLGregorianCalendar(format.parse(request.getParameter("checkIn")));
			checkOut = DateConverter.toXMLGregorianCalendar(format.parse(request.getParameter("checkOut")));
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return port1.getFilteredRooms(city, adults, children, checkIn, checkOut);
	}
	
	@POST
	@Path("/reserveRoom")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public ReservationType reserveRoom(ReservationDTO reservationDto) {
		String email = reservationDto.getEmail();
		int roomId = reservationDto.getRoomId();
		//DateFormat format = new SimpleDateFormat("yyyyy-MM-dd");
		//System.out.println(reservationDto.getCheckInDate().toString());
		//System.out.println(reservationDto.getCheckOutDate().toString());
		XMLGregorianCalendar checkInDate = null, checkOutDate = null;
		try {
			checkInDate = DateConverter.toXMLGregorianCalendar(reservationDto.getCheckInDate());
			checkOutDate = DateConverter.toXMLGregorianCalendar(reservationDto.getCheckOutDate());
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return port1.reserveRoom(email, roomId, checkInDate, checkOutDate);
			
	}
	
	@GET
	@Path("/getReservationsByUser")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<ReservationType> getReservationsByUser(@Context HttpServletRequest request) {
		int userId = Integer.parseInt(request.getParameter("userId"));
		return port1.getAllReservationsByUser(userId);
	}
	
	@GET
	@Path("/getReservationsByUserEmail")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public List<ReservationType> getReservationsByUserEmail(@Context HttpServletRequest request) {
		String email = request.getParameter("email");
		return port1.getAllReservationsByUserEmail(email);
	}
	
	@PUT
	@Path("/cancelReservation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed("User")
	public ReservationType cancelReservation(int reservationId) {
		//int reservationId = reservationDto.getId();
		return port1.cancelReservation(reservationId);
	}
	
	
}
