package handler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class CustomHandlerResolver implements HandlerResolver {

	@Override
	public List<Handler> getHandlerChain(PortInfo arg0) {
		List<Handler> handlerChain = new ArrayList<>();
		handlerChain.add(new AddressHandler());
		return handlerChain;
	}
	
	
	
	
}
