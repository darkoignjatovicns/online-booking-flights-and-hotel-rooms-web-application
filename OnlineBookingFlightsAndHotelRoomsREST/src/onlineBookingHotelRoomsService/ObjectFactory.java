
package onlineBookingHotelRoomsService;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the onlineBookingHotelRoomsService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: onlineBookingHotelRoomsService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelReservation }
     * 
     */
    public CancelReservation createCancelReservation() {
        return new CancelReservation();
    }

    /**
     * Create an instance of {@link CancelReservationResponse }
     * 
     */
    public CancelReservationResponse createCancelReservationResponse() {
        return new CancelReservationResponse();
    }

    /**
     * Create an instance of {@link ReservationType }
     * 
     */
    public ReservationType createReservationType() {
        return new ReservationType();
    }

    /**
     * Create an instance of {@link GetAllReservationsByUser }
     * 
     */
    public GetAllReservationsByUser createGetAllReservationsByUser() {
        return new GetAllReservationsByUser();
    }

    /**
     * Create an instance of {@link GetAllReservationsByUserEmail }
     * 
     */
    public GetAllReservationsByUserEmail createGetAllReservationsByUserEmail() {
        return new GetAllReservationsByUserEmail();
    }

    /**
     * Create an instance of {@link GetAllReservationsByUserEmailResponse }
     * 
     */
    public GetAllReservationsByUserEmailResponse createGetAllReservationsByUserEmailResponse() {
        return new GetAllReservationsByUserEmailResponse();
    }

    /**
     * Create an instance of {@link GetAllReservationsByUserResponse }
     * 
     */
    public GetAllReservationsByUserResponse createGetAllReservationsByUserResponse() {
        return new GetAllReservationsByUserResponse();
    }

    /**
     * Create an instance of {@link GetFilteredRooms }
     * 
     */
    public GetFilteredRooms createGetFilteredRooms() {
        return new GetFilteredRooms();
    }

    /**
     * Create an instance of {@link GetFilteredRoomsResponse }
     * 
     */
    public GetFilteredRoomsResponse createGetFilteredRoomsResponse() {
        return new GetFilteredRoomsResponse();
    }

    /**
     * Create an instance of {@link RoomType }
     * 
     */
    public RoomType createRoomType() {
        return new RoomType();
    }

    /**
     * Create an instance of {@link ReserveRoom }
     * 
     */
    public ReserveRoom createReserveRoom() {
        return new ReserveRoom();
    }

    /**
     * Create an instance of {@link ReserveRoomResponse }
     * 
     */
    public ReserveRoomResponse createReserveRoomResponse() {
        return new ReserveRoomResponse();
    }

    /**
     * Create an instance of {@link SignIn }
     * 
     */
    public SignIn createSignIn() {
        return new SignIn();
    }

    /**
     * Create an instance of {@link SignInResponse }
     * 
     */
    public SignInResponse createSignInResponse() {
        return new SignInResponse();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link SignUp }
     * 
     */
    public SignUp createSignUp() {
        return new SignUp();
    }

    /**
     * Create an instance of {@link SignUpResponse }
     * 
     */
    public SignUpResponse createSignUpResponse() {
        return new SignUpResponse();
    }

    /**
     * Create an instance of {@link RoleType }
     * 
     */
    public RoleType createRoleType() {
        return new RoleType();
    }

    /**
     * Create an instance of {@link HotelType }
     * 
     */
    public HotelType createHotelType() {
        return new HotelType();
    }

    /**
     * Create an instance of {@link RoomStatusType }
     * 
     */
    public RoomStatusType createRoomStatusType() {
        return new RoomStatusType();
    }

}
