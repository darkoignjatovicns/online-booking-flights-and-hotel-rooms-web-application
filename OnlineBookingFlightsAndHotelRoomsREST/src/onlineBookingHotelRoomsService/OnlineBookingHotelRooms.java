package onlineBookingHotelRoomsService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.5-jbossorg-1
 * 2019-07-01T16:24:13.526-07:00
 * Generated source version: 3.2.5-jbossorg-1
 *
 */
@WebService(targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", name = "OnlineBookingHotelRooms")
@XmlSeeAlso({ObjectFactory.class})
public interface OnlineBookingHotelRooms {

    @WebMethod
    @RequestWrapper(localName = "getAllReservationsByUserEmail", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetAllReservationsByUserEmail")
    @ResponseWrapper(localName = "getAllReservationsByUserEmailResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetAllReservationsByUserEmailResponse")
    @WebResult(name = "out", targetNamespace = "")
    public java.util.List<onlineBookingHotelRoomsService.ReservationType> getAllReservationsByUserEmail(
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @RequestWrapper(localName = "signUp", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.SignUp")
    @ResponseWrapper(localName = "signUpResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.SignUpResponse")
    @WebResult(name = "out", targetNamespace = "")
    public onlineBookingHotelRoomsService.UserType signUp(
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email,
        @WebParam(name = "username", targetNamespace = "")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @RequestWrapper(localName = "reserveRoom", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.ReserveRoom")
    @ResponseWrapper(localName = "reserveRoomResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.ReserveRoomResponse")
    @WebResult(name = "out", targetNamespace = "")
    public onlineBookingHotelRoomsService.ReservationType reserveRoom(
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email,
        @WebParam(name = "roomId", targetNamespace = "")
        int roomId,
        @WebParam(name = "checkInDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar checkInDate,
        @WebParam(name = "checkOutDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar checkOutDate
    );

    @WebMethod
    @RequestWrapper(localName = "getAllReservationsByUser", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetAllReservationsByUser")
    @ResponseWrapper(localName = "getAllReservationsByUserResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetAllReservationsByUserResponse")
    @WebResult(name = "out", targetNamespace = "")
    public java.util.List<onlineBookingHotelRoomsService.ReservationType> getAllReservationsByUser(
        @WebParam(name = "userId", targetNamespace = "")
        int userId
    );

    @WebMethod(action = "http://www.example.org/OnlineBookingHotelRooms/signIn")
    @RequestWrapper(localName = "signIn", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.SignIn")
    @ResponseWrapper(localName = "signInResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.SignInResponse")
    @WebResult(name = "out", targetNamespace = "")
    public onlineBookingHotelRoomsService.UserType signIn(
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @RequestWrapper(localName = "cancelReservation", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.CancelReservation")
    @ResponseWrapper(localName = "cancelReservationResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.CancelReservationResponse")
    @WebResult(name = "out", targetNamespace = "")
    public onlineBookingHotelRoomsService.ReservationType cancelReservation(
        @WebParam(name = "reservationId", targetNamespace = "")
        int reservationId
    );

    @WebMethod
    @RequestWrapper(localName = "getFilteredRooms", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetFilteredRooms")
    @ResponseWrapper(localName = "getFilteredRoomsResponse", targetNamespace = "http://www.example.org/OnlineBookingHotelRooms/", className = "onlineBookingHotelRoomsService.GetFilteredRoomsResponse")
    @WebResult(name = "out", targetNamespace = "")
    public java.util.List<onlineBookingHotelRoomsService.RoomType> getFilteredRooms(
        @WebParam(name = "city", targetNamespace = "")
        java.lang.String city,
        @WebParam(name = "adults", targetNamespace = "")
        int adults,
        @WebParam(name = "children", targetNamespace = "")
        int children,
        @WebParam(name = "checkIn", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar checkIn,
        @WebParam(name = "checkOut", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar checkOut
    );
}
