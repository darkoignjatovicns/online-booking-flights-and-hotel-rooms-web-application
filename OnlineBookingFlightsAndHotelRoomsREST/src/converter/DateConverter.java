package converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateConverter {
	
	/*

	* Converts XMLGregorianCalendar to java.util.Date in Java

	*/

	public static Date toDate(XMLGregorianCalendar calendar){

		if(calendar == null) {
	
			return null;
	
		}
	
		return calendar.toGregorianCalendar().getTime();

	}
	
	
	/*

	* Converts java.util.Date to javax.xml.datatype.XMLGregorianCalendar

	*/
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){
		
		if(date == null) {
			return null;
		}
		
		// Set default GMT+0:00 time zone
		TimeZone timeZone;
		timeZone = TimeZone.getTimeZone("GMT+0:00");
		TimeZone.setDefault(timeZone);
		
		DateFormat format = new SimpleDateFormat("yyyyy-MM-dd");
		format.setTimeZone(timeZone);;
		
		
		
		XMLGregorianCalendar gDateFormatted1;
		try {
			gDateFormatted1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
			System.out.println("Formatted using SimpleDateFormat: " + gDateFormatted1.toString());
			return gDateFormatted1;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}
			
	}
	
	public static XMLGregorianCalendar toXMLGregorianCalendar2(Date date){
		
		if(date == null) {
			return null;
		}
		
		GregorianCalendar gCalendar = new GregorianCalendar();

		gCalendar.setTime(date);

		XMLGregorianCalendar xmlCalendar = null;

		try {

			xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);

		} catch (DatatypeConfigurationException ex) {

			//Logger.getLogger(StringReplace.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("Nesto ne valja");
			return null;

		}

		return xmlCalendar;

	}
	
	
	
	
}
