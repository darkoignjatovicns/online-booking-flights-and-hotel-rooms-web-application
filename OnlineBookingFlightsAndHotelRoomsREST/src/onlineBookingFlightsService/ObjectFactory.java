
package onlineBookingFlightsService;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the onlineBookingFlightsService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: onlineBookingFlightsService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelFlightReservation }
     * 
     */
    public CancelFlightReservation createCancelFlightReservation() {
        return new CancelFlightReservation();
    }

    /**
     * Create an instance of {@link CancelFlightReservationResponse }
     * 
     */
    public CancelFlightReservationResponse createCancelFlightReservationResponse() {
        return new CancelFlightReservationResponse();
    }

    /**
     * Create an instance of {@link FlightReservationType }
     * 
     */
    public FlightReservationType createFlightReservationType() {
        return new FlightReservationType();
    }

    /**
     * Create an instance of {@link GetAllFlightReservationByUserEmail }
     * 
     */
    public GetAllFlightReservationByUserEmail createGetAllFlightReservationByUserEmail() {
        return new GetAllFlightReservationByUserEmail();
    }

    /**
     * Create an instance of {@link GetAllFlightReservationByUserEmailResponse }
     * 
     */
    public GetAllFlightReservationByUserEmailResponse createGetAllFlightReservationByUserEmailResponse() {
        return new GetAllFlightReservationByUserEmailResponse();
    }

    /**
     * Create an instance of {@link GetAllReservationFlightsByUser }
     * 
     */
    public GetAllReservationFlightsByUser createGetAllReservationFlightsByUser() {
        return new GetAllReservationFlightsByUser();
    }

    /**
     * Create an instance of {@link GetAllReservationFlightsByUserResponse }
     * 
     */
    public GetAllReservationFlightsByUserResponse createGetAllReservationFlightsByUserResponse() {
        return new GetAllReservationFlightsByUserResponse();
    }

    /**
     * Create an instance of {@link GetAllReservedFlightsByUser }
     * 
     */
    public GetAllReservedFlightsByUser createGetAllReservedFlightsByUser() {
        return new GetAllReservedFlightsByUser();
    }

    /**
     * Create an instance of {@link GetAllReservedFlightsByUserResponse }
     * 
     */
    public GetAllReservedFlightsByUserResponse createGetAllReservedFlightsByUserResponse() {
        return new GetAllReservedFlightsByUserResponse();
    }

    /**
     * Create an instance of {@link GetFilteredFlights }
     * 
     */
    public GetFilteredFlights createGetFilteredFlights() {
        return new GetFilteredFlights();
    }

    /**
     * Create an instance of {@link GetFilteredFlightsResponse }
     * 
     */
    public GetFilteredFlightsResponse createGetFilteredFlightsResponse() {
        return new GetFilteredFlightsResponse();
    }

    /**
     * Create an instance of {@link FlightType }
     * 
     */
    public FlightType createFlightType() {
        return new FlightType();
    }

    /**
     * Create an instance of {@link ReserveFlight }
     * 
     */
    public ReserveFlight createReserveFlight() {
        return new ReserveFlight();
    }

    /**
     * Create an instance of {@link ReserveFlightResponse }
     * 
     */
    public ReserveFlightResponse createReserveFlightResponse() {
        return new ReserveFlightResponse();
    }

    /**
     * Create an instance of {@link SignIn }
     * 
     */
    public SignIn createSignIn() {
        return new SignIn();
    }

    /**
     * Create an instance of {@link SignInResponse }
     * 
     */
    public SignInResponse createSignInResponse() {
        return new SignInResponse();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link SignUp }
     * 
     */
    public SignUp createSignUp() {
        return new SignUp();
    }

    /**
     * Create an instance of {@link SignUpResponse }
     * 
     */
    public SignUpResponse createSignUpResponse() {
        return new SignUpResponse();
    }

    /**
     * Create an instance of {@link RoleType }
     * 
     */
    public RoleType createRoleType() {
        return new RoleType();
    }

    /**
     * Create an instance of {@link PlaneType }
     * 
     */
    public PlaneType createPlaneType() {
        return new PlaneType();
    }

}
