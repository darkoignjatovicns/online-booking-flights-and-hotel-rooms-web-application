package onlineBookingFlightsService;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.5-jbossorg-1
 * 2019-07-01T16:23:06.619-07:00
 * Generated source version: 3.2.5-jbossorg-1
 *
 */
@WebServiceClient(name = "OnlineBookingFlights",
                  wsdlLocation = "http://localhost:8080/OnlineBookingFlightsServer/OnlineBookingFlights?wsdl",
                  targetNamespace = "http://www.example.org/OnlineBookingFlights/")
public class OnlineBookingFlights_Service extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.example.org/OnlineBookingFlights/", "OnlineBookingFlights");
    public final static QName OnlineBookingFlightsImplPort = new QName("http://www.example.org/OnlineBookingFlights/", "OnlineBookingFlightsImplPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/OnlineBookingFlightsServer/OnlineBookingFlights?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(OnlineBookingFlights_Service.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://localhost:8080/OnlineBookingFlightsServer/OnlineBookingFlights?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public OnlineBookingFlights_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public OnlineBookingFlights_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public OnlineBookingFlights_Service() {
        super(WSDL_LOCATION, SERVICE);
    }

    public OnlineBookingFlights_Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public OnlineBookingFlights_Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public OnlineBookingFlights_Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns OnlineBookingFlights
     */
    @WebEndpoint(name = "OnlineBookingFlightsImplPort")
    public OnlineBookingFlights getOnlineBookingFlightsImplPort() {
        return super.getPort(OnlineBookingFlightsImplPort, OnlineBookingFlights.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns OnlineBookingFlights
     */
    @WebEndpoint(name = "OnlineBookingFlightsImplPort")
    public OnlineBookingFlights getOnlineBookingFlightsImplPort(WebServiceFeature... features) {
        return super.getPort(OnlineBookingFlightsImplPort, OnlineBookingFlights.class, features);
    }

}
