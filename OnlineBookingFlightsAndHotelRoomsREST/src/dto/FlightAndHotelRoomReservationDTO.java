package dto;

import java.util.Date;

public class FlightAndHotelRoomReservationDTO {
	
	private int id;
	private String email;
	//private Date checkInDate;
	
	// Flight attributes
	private int adultsPassengers;
	private int childrenPassengers;
	private int flightId;
	
	// Hotel room attributes
	private Date checkInDate;
	private Date checkOutDate;
	private int totalPrice;
	private int roomId;
	private int adults;
	private int children;
	public FlightAndHotelRoomReservationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAdultsPassengers() {
		return adultsPassengers;
	}
	public void setAdultsPassengers(int adultsPassengers) {
		this.adultsPassengers = adultsPassengers;
	}
	public int getChildrenPassengers() {
		return childrenPassengers;
	}
	public void setChildrenPassengers(int childrenPassengers) {
		this.childrenPassengers = childrenPassengers;
	}
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public int getAdults() {
		return adults;
	}
	public void setAdults(int adults) {
		this.adults = adults;
	}
	public int getChildren() {
		return children;
	}
	public void setChildren(int children) {
		this.children = children;
	}
	
	
	
	
	
}