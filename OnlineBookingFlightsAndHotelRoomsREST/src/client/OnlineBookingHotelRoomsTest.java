package client;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import handler.CustomHandlerResolver;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms_Service;
import onlineBookingHotelRoomsService.ReservationType;
import onlineBookingHotelRoomsService.RoomType;

public class OnlineBookingHotelRoomsTest {
	
	public static void main(String[] args) {
		System.out.println("******************************");
		System.out.println("Create Web service Client");
		
		OnlineBookingHotelRooms_Service service1 = new OnlineBookingHotelRooms_Service();
		service1.setHandlerResolver(new CustomHandlerResolver());
		System.out.println("Create Web service");
		OnlineBookingHotelRooms port1 = service1.getOnlineBookingHotelRoomsImplPort();
		
		System.out.println("Call Web Service Operation");
		
		/*Client client = ClientProxy.getClient(port1);
		Endpoint cxfEndpoint = client.getEndpoint();

		Map<String, Object> outProps = new HashMap<String, Object>();
		outProps.put("action", "Encrypt Signature Timestamp");
		outProps.put("user", "client");
		outProps.put("passwordCallbackClass", "handlers.ClientKeystorePasswordCallback");
		outProps.put("encryptionPropFile", "resources/clientKeystore.properties");
		outProps.put("encryptionUser", "server");
		outProps.put("encryptionKeyIdentifier", "DirectReference");
		outProps.put("signatureKeyIdentifier", "DirectReference");
		outProps.put("signaturePropFile", "resources/clientKeystore.properties");
		outProps.put("signatureParts",
				"{}{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Timestamp;{}{http://schemas.xmlsoap.org/soap/envelope/}Body;");
		

		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps); // request
		cxfEndpoint.getOutInterceptors().add(wssOut);

		
		
		Map<String, Object> inProps = new HashMap<String, Object>();
		inProps.put("action", "Encrypt");
		inProps.put("passwordCallbackClass", "handlers.ClientKeystorePasswordCallback");
		inProps.put("decryptionPropFile", "resources/clientKeystore.properties");
		WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps); // response
		 cxfEndpoint.getInInterceptors().add(wssIn);*/
		
		
		// signIn
		//UserType user = port1.signIn("darko@gmail.com", "Dark021.");
		//System.out.println(user.getId() + " " +user.getEmail() + " " + user.getRole().getRoleName() + " " + user.getCreatedDate());
		
		// signUp
		//UserType user1 = port1.signUp("John", "Doe", "john@gmail.com", "john", "John021.");
		//System.out.println(user1.getId() + " " + user1.getCreatedDate() + " " + user1.getFirstName());
		
		// getFilteredRooms
		DateFormat format = new SimpleDateFormat("yyyyy-MM-dd");
		
		XMLGregorianCalendar checkIn = null;
		XMLGregorianCalendar checkOut = null;
		try {
			checkIn = DateConverter.toXMLGregorianCalendar(format.parse("2019-06-14"));
			checkOut =  DateConverter.toXMLGregorianCalendar(format.parse("2019-06-19"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Gregorijan cal.");
		}
		
		
		List<RoomType> filteredRoomsList = port1.getFilteredRooms("Novi Sad", 2, 0, checkIn , checkOut);
		System.out.println("\nFree Hotel rooms\n");
		for(RoomType roomType : filteredRoomsList) {
			System.out.println("Hotel: " + roomType.getHotel().getHotelName() + 
								", AdultsMax: " + roomType.getAdultsMax() + 
								", ChildrenMax: " + roomType.getChildrenMax());
		}
		
		// reserveRoom
		//ReservationType reservationType= port1.reserveRoom(8, 1, checkIn, checkOut);
		//System.out.println("\nRoom successfuly reservation: " + reservationType.getReservationDate() + "\n");
		
		// getReservationByUser
		List<ReservationType> reservationTypeList = port1.getAllReservationsByUser(8);
		System.out.println("\nReservation by user John Doe: ");
		for(ReservationType res : reservationTypeList) {
			System.out.println("\n" + res.getId() + ", " + res.getRoom().getHotel().getHotelName() + ", " + res.getCheckInDate() + ", " + res.getRoom().getAdultsMax() + "\n");
		}
		
		// Cancel reservation
		//ReservationType canceledReservation = port1.cancelReservation(31);
		//System.out.println("\nCanceled resevation with id: " + canceledReservation.getId() + ", " + canceledReservation.getCanceledReservationDate());
		
	}
}
