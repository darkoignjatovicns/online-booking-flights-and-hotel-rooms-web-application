package client;

import java.util.List;

import helper.OnlineBookingFlightsInitializer;
import onlineBookingFlightsService.FlightReservationType;
import onlineBookingFlightsService.OnlineBookingFlights;
import onlineBookingFlightsService.UserType;

public class OnlineBookingFlightsTest {
	
	public static void main(String[] args) {
	
		System.out.println("*********************");
		System.out.println("Create Web Service Client");
		
		//OnlineBookingFlights_Service service1 = new OnlineBookingFlights_Service();
		//service1.setHandlerResolver(new CustomBookingFlightsHandlerResolver());		
		//OnlineBookingFlights port1 = service1.getOnlineBookingFlightsImplPort(); 
		
		OnlineBookingFlights port1 = OnlineBookingFlightsInitializer.getOnlineBookingFlightsPort();
		
		System.out.println("Call Web Service Operations");
		
		//Sign up
		/*UserType userType = port1.signUp("Gucci", "Maid", "gucci@gmail.com", "gucci", "Gucci021.");
		System.out.println("User email: " + userType.getEmail() + " User Id: " + userType.getId());*/
	
		//Sign In
		UserType currentUser = port1.signIn("gucci@gmail.com", "Gucci021.");
		System.out.println("User id: " + currentUser.getId());
		
		//GetFilteredFlights
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date departureDate = null;
		try  {
			departureDate = sdf.parse("2019-06-27");
			System.out.println(departureDate);
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		String from = "Novi Sad";
		String  to = "London";
		XMLGregorianCalendar convertedDepartureDate = DateConverter.toXMLGregorianCalendar(departureDate);
		System.out.println(convertedDepartureDate);
	
		List<FlightType> filteredFlights = port1.getFilteredFlights(from,to,convertedDepartureDate,null);
		System.out.println("Filghts which departure from \"" + from + "\" to destination \"" + to + "\"" );
		for(FlightType f : filteredFlights) {
			//System.out.print(f.getFrom() + " " + f.getTo() + " " + f.getDepartureDate() + " " + f.getDepartureTime());
			System.out.println(f.getFrom());
			System.out.println(f.getTo());
			System.out.println(f.getDepartureDate());
			System.out.println(f.getDepartureTime());
		}*/
		
		
		// Reserve flight
		/*FlightReservationType flightReservationType = port1.reserveFlight(currentUser.getId(), 1 , 2, 0);
		System.out.println("Flight Reservation have successfully made\n");
		System.out.println("Reservation flight details: \n");
		System.out.println("Adults: " + flightReservationType.getAdults());
		System.out.println("Children: " + flightReservationType.getChildren());
		System.out.println("Price: " + flightReservationType.getTotalPrice());
		System.out.println("Reservation Date: " + flightReservationType.getReservationDate());
		System.out.println("\nUser details:\n");
		System.out.println(flightReservationType.getUser().getEmail());
		System.out.println("\nFlight Reservation:\n");
		System.out.println(flightReservationType.getFlight().getFrom());
		System.out.println(flightReservationType.getFlight().getTo());
		System.out.println(flightReservationType.getFlight().getDepartureDate());
		System.out.println(flightReservationType.getFlight().getDepartureTime());
		System.out.println(flightReservationType.getFlight().getExpectedTravelTime());*/
		
		
		// List of all flight reservation by User
		List<FlightReservationType> flightReservationsByUser = port1.getAllReservedFlightsByUser(currentUser.getId());
		int counter =  1;
		for(FlightReservationType flightReservation : flightReservationsByUser) {
			System.out.println(counter + ". Reservation flight details: \n");
			System.out.print("Reservation id: " + flightReservation.getId());
			System.out.println("Adults: " + flightReservation.getAdults());
			System.out.println("Children: " + flightReservation.getChildren());
			System.out.println("Price: " + flightReservation.getTotalPrice());
			System.out.println("Reservation Date: " + flightReservation.getReservationDate());
			System.out.println("\nUser details:\n");
			System.out.println(flightReservation.getUser().getEmail());
			System.out.println("\nFlight Reservation:\n");
			System.out.println(flightReservation.getFlight().getFrom());
			System.out.println(flightReservation.getFlight().getTo());
			System.out.println(flightReservation.getFlight().getDepartureDate());
			System.out.println(flightReservation.getFlight().getDepartureTime());
			System.out.println(flightReservation.getFlight().getExpectedTravelTime());
		}
		
		
		// Cancel Flight Reservation
		/*FlightReservationType canceledFlightReservation = port1.cancelFlightReservation(1);
		System.out.println("Reservation have been made successfully canceled");
		System.out.println("Reservation id: " + canceledFlightReservation.getId());
		System.out.println("Resevation canceled date: " +  canceledFlightReservation.getCanceledReservation());
		*/
		
		
	}
	
}
