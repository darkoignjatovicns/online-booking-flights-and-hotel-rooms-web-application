package security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import helper.Initializer;
import onlineBookingHotelRoomsService.OnlineBookingHotelRooms;
import onlineBookingHotelRoomsService.UserType;



@Provider
public class SecurityInterceptor implements ContainerRequestFilter {

	OnlineBookingHotelRooms port1 = Initializer.getOnlineBookingHotelRoomsPort();
	//OnlineBookingFlights port1 = OnlineBookingFlightsInitializer.getOnlineBookingFlightsPort();

	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHORIZATION_SCHEME = "Basic";
	private static final ServerResponse ACCESS_DENIED = new ServerResponse("ACCESS_DENIED", 401, new Headers<Object>());
	private static final ServerResponse SERVER_ERROR = new ServerResponse("SERVER_ERROR", 500, new Headers<Object>());
	private static final ServerResponse ACCESS_FORBIDDEN = new ServerResponse("ACCESS_FORBIDDEN", 403,
			new Headers<Object>());

	@Override
	public void filter(ContainerRequestContext context) throws IOException {
		
		System.out.println("Filter is being started");
		
		ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) context
				.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");

		Method method = methodInvoker.getMethod();
		if (method.getName().equals("signIn") || method.getName().equals("signUp")) {
			
		} else {
			MultivaluedMap<String, String> mvmap = context.getHeaders();
			List<String> aproperies = mvmap.get(AUTHORIZATION_PROPERTY);

			if (aproperies == null || aproperies.isEmpty()) {
				System.out.println("FILTER ACCESS DENIED 1");
				context.abortWith(ACCESS_DENIED);
				return;
			}

			String encodedUserAndPass = aproperies.get(0).replaceFirst(AUTHORIZATION_SCHEME + " ", "");

			String decodedUserAndPass = new String(new Base64().decode(encodedUserAndPass));
			// user:pass

			StringTokenizer tokenizer = new StringTokenizer(decodedUserAndPass, ":");
			String email = tokenizer.nextToken();
			String password = tokenizer.nextToken();
			
			UserType user = port1.signIn(email, password);
			
			if(user == null) {
				System.out.println("FILTER ACCESS DENIED 2");

				context.abortWith(ACCESS_DENIED);
				return;
			}

			if (user.getRole().getRoleName().equals("Manager") || user.getRole().getRoleName().equals("User")) {
				
			}else {
				System.out.println("FILTER ACCESS DENIED 3");

				context.abortWith(ACCESS_DENIED);
				return;
			}

			if (method.isAnnotationPresent(RolesAllowed.class)) {
				RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
				Set<String> skupRola = new HashSet<String>(Arrays.asList(rolesAllowed.value()));

				String role = user.getRole().getRoleName();
				
				if (!skupRola.contains(role)) {
					System.out.println(" FILTER ACCESS FORBIDDEN 4");

					context.abortWith(ACCESS_FORBIDDEN);
					return;
				}

			}
			System.out.println("The request has been passed successfully! The filter was ended");
		}
	}

}