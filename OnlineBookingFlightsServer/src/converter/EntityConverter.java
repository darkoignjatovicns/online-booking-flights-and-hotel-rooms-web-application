package converter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Flight;
import model.Plane;
import model.Reservation;
import model.Role;
import model.User;
import service.FlightReservationType;
import service.FlightType;
import service.PlaneType;
import service.RoleType;
import service.UserType;

public class EntityConverter {
	
	@PersistenceContext
	EntityManager em;
	
	
	/* Convert User To UserType */
	public static UserType convertUserToUserType(User user) {
		
		// Covert User to UserType
		UserType userType = new UserType();
		userType.setId(user.getId());
		userType.setFirstName(user.getFirstName());
		userType.setLastName(user.getLastName());
		userType.setEmail(user.getEmail());
		userType.setPassword(user.getPassword());
		userType.setCreatedDate(DateConverter.toXMLGregorianCalendar(user.getCreatedDate()));
		userType.setDeletedDate(DateConverter.toXMLGregorianCalendar(user.getDeletedDate()));
		userType.setLastUpdateDate(DateConverter.toXMLGregorianCalendar(user.getLastUpdateDate()));
		userType.setRole(convertRoleToRoleType(user.getRole()));
		//userType.setImage(user.getImage());
		//userType.setImagePut(user.getImagePut());
	
		return userType;
	}
	
	/* Convert Role to RoleType */
	public static RoleType convertRoleToRoleType(Role role) {
		RoleType roleType = new RoleType();
		roleType.setId(role.getId());
		roleType.setRoleName(role.getRoleName());
		
		return  roleType;
	}
	
	
	/* Convert Flight to FlightType */
	public static FlightType convertFlightToFlightType(Flight flight) {
		
		FlightType flightType = new FlightType();
		flightType.setId(flight.getId());
		flightType.setFrom(flight.getFrom());
		flightType.setTo(flight.getTo());
		flightType.setDepartureDate(DateConverter.toXMLGregorianCalendar(flight.getDepartureDate()));
		flightType.setDepartureTime(DateConverter.toXMLGregorianCalendarOnlyTime(flight.getDepartureTime()));
		flightType.setExpectedTravelTime(DateConverter.toXMLGregorianCalendarOnlyTime(flight.getExpectedTravelTime()));
		flightType.setPlane(convertPlaneToPlaneType(flight.getPlane()));
		flightType.setReturnDate(DateConverter.toXMLGregorianCalendar(flight.getReturnDate()));
		flightType.setReturnTime(DateConverter.toXMLGregorianCalendarOnlyTime(flight.getReturnTime()));
		flightType.setPrice(flight.getPrice());
		
		return flightType;
		
	}
	
	/* Convert Plane to PlaneType */
	public static PlaneType convertPlaneToPlaneType(Plane plane) {
		
		PlaneType planeType = new PlaneType();
		planeType.setId(plane.getId());
		planeType.setName(plane.getName());
		//planeType.setNumberOfSeats(plane.getNumberOfSeats());
		planeType.setType(plane.getType());
		planeType.setCompany(plane.getCompany());
		
		return planeType;
	}
	
	/* Convert Reservation to FlightReservationType */
	public static FlightReservationType convertReservationToFlightReservationType(Reservation reservation) {
		
		FlightReservationType flightReservationType = new FlightReservationType();
		flightReservationType.setId(reservation.getId());
		flightReservationType.setUser(convertUserToUserType(reservation.getUser()));
		flightReservationType.setFlight(convertFlightToFlightType(reservation.getFlight()));
		flightReservationType.setReservationDate(DateConverter.toXMLGregorianCalendar(reservation.getReservationDate()));
		flightReservationType.setCanceledReservation(DateConverter.toXMLGregorianCalendar(reservation.getCanceledReservation()));
		flightReservationType.setTotalPrice(reservation.getTotalPrice());
		flightReservationType.setAdults(reservation.getAdults());
		flightReservationType.setChildren(reservation.getChildren());
		
		return flightReservationType;
	}
	
	
	
	/* Convert List<Reservation> to List<FlightReservationType> */
	
	public static List<FlightReservationType> convertReservationListToFlightReservationType(List<Reservation> reservations) {
		
		List<FlightReservationType> flightReservationTypeList = new ArrayList<>();
		
		for(Reservation reservation : reservations) {
			flightReservationTypeList.add(convertReservationToFlightReservationType(reservation));
		}
		
		return flightReservationTypeList;
		
	}
	
	
	/* Convert List<Flight> to List<FlightType> */
	public static List<FlightType> convertFlightListToFlightTypeList(List<Flight> flights) {
		
		List<FlightType> flightTypeList = new ArrayList<>();
		
		for(Flight flight : flights) {
			flightTypeList.add(convertFlightToFlightType(flight));
		}
		
		return flightTypeList;
	}
	
}
