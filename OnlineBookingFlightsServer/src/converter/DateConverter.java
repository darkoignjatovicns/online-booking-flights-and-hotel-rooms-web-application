package converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.sun.glass.ui.Pixels.Format;

public class DateConverter {
	
	/*

	* Converts XMLGregorianCalendar to java.util.Date in Java

	*/

	public static Date toDate(XMLGregorianCalendar calendar){

		if(calendar == null) {
	
			return null;
	
		}
	
		return calendar.toGregorianCalendar().getTime();

	}
	
	
	/*

	* Converts java.util.Date to javax.xml.datatype.XMLGregorianCalendar

	*/
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){
		
		if(date == null) {
			return null;
		}
		
		// Set default GMT+0:00 time zone
		/*TimeZone timeZone;
		timeZone = TimeZone.getTimeZone("GMT+0:00");
		TimeZone.setDefault(timeZone);*/
		
		DateFormat format = new SimpleDateFormat("yyyyy-MM-dd");
		//format.setTimeZone(timeZone);;
		
		XMLGregorianCalendar gDateFormatted1;
		try {
			gDateFormatted1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
			System.out.println("Formatted using SimpleDateFormat(Backend) :  " + gDateFormatted1.toString());
			return gDateFormatted1;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public static XMLGregorianCalendar toXMLGregorianCalendarOnlyTime(Date date){
		
		if(date == null) {
			return null;
		}
		
		// Set default GMT+0:00 time zone
		/*TimeZone timeZone;
		timeZone = TimeZone.getTimeZone("GMT+0:00");
		TimeZone.setDefault(timeZone);*/
		
		DateFormat format = new SimpleDateFormat("HH:mm:ss");
		//format.setTimeZone(timeZone);;
		
		XMLGregorianCalendar gDateFormatted1;
		try {
			gDateFormatted1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
			System.out.println("Formatted using SimpleDateFormat(Backend) :  " + gDateFormatted1.toString());
			return gDateFormatted1;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	/* Get only time from full Date */
	public static Date getTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		//System.out.println(sdf.format(date));
		try {
			return sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	
}
