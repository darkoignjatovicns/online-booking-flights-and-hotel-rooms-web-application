package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the flight database table.
 * 
 */
@Entity
@NamedQuery(name="Flight.findAll", query="SELECT f FROM Flight f")
public class Flight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="departure_date")
	private Date departureDate;

	@Temporal(TemporalType.TIME)
	@Column(name="departure_time")
	private Date departureTime;

	@Temporal(TemporalType.TIME)
	@Column(name="expected_travel_time")
	private Date expectedTravelTime;

	private String from;

	private Integer price;

	@Temporal(TemporalType.DATE)
	@Column(name="return_date")
	private Date returnDate;

	@Temporal(TemporalType.TIME)
	@Column(name="return_time")
	private Date returnTime;

	private String to;

	//bi-directional many-to-one association to Plane
	@ManyToOne
	private Plane plane;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="flight")
	private List<Reservation> reservations;

	public Flight() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getDepartureTime() {
		return this.departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getExpectedTravelTime() {
		return this.expectedTravelTime;
	}

	public void setExpectedTravelTime(Date expectedTravelTime) {
		this.expectedTravelTime = expectedTravelTime;
	}

	public String getFrom() {
		return this.from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Integer getPrice() {
		return this.price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public Date getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}

	public String getTo() {
		return this.to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Plane getPlane() {
		return this.plane;
	}

	public void setPlane(Plane plane) {
		this.plane = plane;
	}

	public List<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setFlight(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setFlight(null);

		return reservation;
	}

}