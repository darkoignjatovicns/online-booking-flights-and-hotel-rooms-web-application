package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the reservation database table.
 * 
 */
@Entity
@NamedQuery(name="Reservation.findAll", query="SELECT r FROM Reservation r")
public class Reservation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer adults;

	@Temporal(TemporalType.DATE)
	@Column(name="canceled_reservation")
	private Date canceledReservation;

	@Temporal(TemporalType.DATE)
	@Column(name="check_in_date")
	private Date checkInDate;

	private Integer children;

	@Temporal(TemporalType.DATE)
	@Column(name="reservation_date")
	private Date reservationDate;

	@Column(name="total_price")
	private Integer totalPrice;

	//bi-directional many-to-one association to Flight
	@ManyToOne
	private Flight flight;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	public Reservation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdults() {
		return this.adults;
	}

	public void setAdults(Integer adults) {
		this.adults = adults;
	}

	public Date getCanceledReservation() {
		return this.canceledReservation;
	}

	public void setCanceledReservation(Date canceledReservation) {
		this.canceledReservation = canceledReservation;
	}

	public Date getCheckInDate() {
		return this.checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Integer getChildren() {
		return this.children;
	}

	public void setChildren(Integer children) {
		this.children = children;
	}

	public Date getReservationDate() {
		return this.reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Integer getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Flight getFlight() {
		return this.flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}