package beans;

import javax.ejb.Remote;

@Remote
public interface OnlineBookingFlightsBeanRemote {
	
	public service.UserType signIn(java.lang.String email, java.lang.String password);
	public java.util.List<service.FlightType> getFilteredFlights(java.lang.String from, java.lang.String to,
			javax.xml.datatype.XMLGregorianCalendar departureDate, javax.xml.datatype.XMLGregorianCalendar returnDate);

	public java.util.List<service.FlightReservationType> getAllReservedFlightsByUser(int userId);

	public service.FlightReservationType cancelFlightReservation(int flightReservationId);

	public service.UserType signUp(java.lang.String firstName, java.lang.String lastName, java.lang.String email,
			java.lang.String username, java.lang.String password);

	public service.FlightReservationType reserveFlight(String email, int flightId, int adults, int children);
	
	public java.util.List<service.FlightReservationType> getAllFlightReservationByUserEmail(java.lang.String email);
	
}
