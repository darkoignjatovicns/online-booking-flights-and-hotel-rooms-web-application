package beans;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.datatype.XMLGregorianCalendar;

import converter.DateConverter;
import converter.EntityConverter;
import model.Flight;
import model.Reservation;
import model.Role;
import model.User;
import service.FlightReservationType;
import service.FlightType;
import service.UserType;

/**
 * Session Bean implementation class OnlineBookingFlightsBean
 */
@Stateless
@LocalBean
public class OnlineBookingFlightsBean implements OnlineBookingFlightsBeanRemote {
	
	
	@PersistenceContext
	EntityManager em;
	
    /**
     * Default constructor. 
     */
    public OnlineBookingFlightsBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public UserType signIn(String email, String password) {
		Query q =  em.createQuery("SELECT u FROM User u WHERE u.email = :email AND u.password = :password");
		q.setParameter("email", email);
		q.setParameter("password", password);
		
		List<User> users =  q.getResultList();
		
		if(users != null) { // Eclipse bug -> There should'n have executed 'if block' if user existed! 
			System.out.println("USERS LIST: " + users);
			User user = users.get(0);
			return EntityConverter.convertUserToUserType(users.get(0));
		}
		
		return null;
	}

	@Override
	public List<FlightType> getFilteredFlights(String from, String to, XMLGregorianCalendar departureDate,
			XMLGregorianCalendar returnDate) {

		String query;
		
		if(returnDate == null) { 
			query = "SELECT f FROM Flight f WHERE f.from = :from"
				+ " AND f.to = :to"
				+ " AND f.departureDate = :departureDate";
		} else {
			query  = "SELECT f FROM Flight f WHERE f.from = :from"
				+ " AND f.to = :to"
				+ " AND f.departureDate = :departureDate"
				+ " AND f.returnDate = :returnDate";
		}
		
		Query q = em.createQuery(query);
		q.setParameter("from", from);
		q.setParameter("to", to);
		q.setParameter("departureDate", DateConverter.toDate(departureDate));
		if(returnDate != null) {
			q.setParameter("returnDate",DateConverter.toDate(returnDate));
		}
		
		return EntityConverter.convertFlightListToFlightTypeList(q.getResultList());
	}

	@Override
	public List<FlightReservationType> getAllReservedFlightsByUser(int userId) { // String email
		
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE r.user.id = :userId AND r.canceledReservation = NULL");
		q.setParameter("userId", userId);
		//List<Reservation> reservations = q.getResultList();
		return EntityConverter.convertReservationListToFlightReservationType(q.getResultList());
		
	}

	@Override
	public FlightReservationType cancelFlightReservation(int flightReservationId) {
		Reservation reservation = em.find(Reservation.class, flightReservationId);
		reservation.setCanceledReservation(new Date());
		em.merge(reservation);
		return EntityConverter.convertReservationToFlightReservationType(reservation);
	}

	@Override
	public UserType signUp(String firstName, String lastName, String email, String username, String password) {
		User user = new User();
		user.setCreatedDate(new Date());
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(password);
		user.setRole((Role)em.find(Role.class, 1));
		em.persist(user);
		
		return EntityConverter.convertUserToUserType(user);
	}

	@Override
	public FlightReservationType reserveFlight(String email, int flightId, int adults, int children) { 
		
		Reservation reservation = new Reservation();
		
		//User user = em.find(User.class, userId);
		Query q = em.createQuery("SELECT u FROM User u WHERE u.email = :email");
		q.setParameter("email", email);
		User user = (User) q.getResultList().get(0);
		
		Flight flight = em.find(Flight.class, flightId);
		reservation.setUser(user);
		reservation.setFlight(flight);
		reservation.setAdults(adults);
		reservation.setChildren(children);
		reservation.setTotalPrice((adults + children) * flight.getPrice());
		reservation.setReservationDate(new Date());
		em.persist(reservation);
		
		return EntityConverter.convertReservationToFlightReservationType(reservation);
		
		
	}

	@Override
	public List<FlightReservationType> getAllFlightReservationByUserEmail(String email) {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE r.user.email = :email AND r.canceledReservation = NULL");
		q.setParameter("email", email);
		//List<Reservation> reservations = q.getResultList();
		return EntityConverter.convertReservationListToFlightReservationType(q.getResultList());
	}

}
