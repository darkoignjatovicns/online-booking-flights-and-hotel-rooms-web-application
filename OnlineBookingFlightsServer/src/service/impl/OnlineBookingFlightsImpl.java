package service.impl;


import javax.inject.Inject;
import javax.jws.HandlerChain;
import javax.jws.WebService;

import beans.OnlineBookingFlightsBean;
import service.OnlineBookingFlights;

@HandlerChain(file="handler-chain.xml")
@WebService(serviceName = "OnlineBookingFlights", endpointInterface = "service.OnlineBookingFlights", targetNamespace = "http://www.example.org/OnlineBookingFlights/")
public class OnlineBookingFlightsImpl implements OnlineBookingFlights {
	
	
	@Inject 
	OnlineBookingFlightsBean onlineBookingFlightsBean;
	
	public service.UserType signIn(java.lang.String email, java.lang.String password) {
		return onlineBookingFlightsBean.signIn(email, password);
	}

	public java.util.List<service.FlightType> getFilteredFlights(java.lang.String from, java.lang.String to,
			javax.xml.datatype.XMLGregorianCalendar departureDate, javax.xml.datatype.XMLGregorianCalendar returnDate) {
		return onlineBookingFlightsBean.getFilteredFlights(from, to, departureDate, returnDate);
	}

	public java.util.List<service.FlightReservationType> getAllReservedFlightsByUser(int userId) {
		return onlineBookingFlightsBean.getAllReservedFlightsByUser(userId);
	}

	public service.FlightReservationType cancelFlightReservation(int flightReservationId) {
		return onlineBookingFlightsBean.cancelFlightReservation(flightReservationId);
	}

	public java.util.List<service.FlightReservationType> getAllFlightReservationByUserEmail(java.lang.String email) {
		return onlineBookingFlightsBean.getAllFlightReservationByUserEmail(email);
	}

	public service.UserType signUp(java.lang.String firstName, java.lang.String lastName, java.lang.String email,
			java.lang.String username, java.lang.String password) {
		return onlineBookingFlightsBean.signUp(firstName, lastName, email, username, password);
	}

	public service.FlightReservationType reserveFlight(java.lang.String email, int flightId, int adults, int children) {
		return onlineBookingFlightsBean.reserveFlight(email, flightId, adults, children);
	}
}